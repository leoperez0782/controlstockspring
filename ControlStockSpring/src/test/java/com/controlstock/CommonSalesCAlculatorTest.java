package com.controlstock;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.controlstock.interactors.CommonSalesCalculator;
import com.controlstock.models.Category;
import com.controlstock.models.Product;
import com.controlstock.models.Sale;
import com.controlstock.models.SaleLine;
import com.controlstock.models.Tax;

class CommonSalesCAlculatorTest {

	static Product productOther;
	static Category categoryBooks;
	static Product productLotery;
	static Category categoryLotery;
	static Tax tax;
	static SaleLine commonLine;
	static SaleLine loteryLine;
	static Sale sale;
	static List<Sale> sales;

	private static final Logger LOGGER = LogManager.getLogger();
	private static final BigDecimal TEN = new BigDecimal(10).setScale(2, RoundingMode.HALF_UP);
	private static final BigDecimal ONE_HUNDRED = new BigDecimal(100).setScale(2, RoundingMode.HALF_UP);
	private static final BigDecimal ONE_HUNDRED_AND_TEN = new BigDecimal(110).setScale(2, RoundingMode.HALF_UP);
	private static final BigDecimal ONE_HUNDRED_AND_TWENTY_ONE = new BigDecimal(121).setScale(2, RoundingMode.HALF_UP);
	
	@BeforeEach
	void setUp() throws Exception {
		productOther = new Product();
		productLotery = new Product();
		categoryBooks = new Category();
		categoryLotery = new Category();
		
		tax = new Tax();
		// add attributes
		categoryBooks.setCategoryName("Libros");
		categoryLotery.setCategoryName("Loterias y quinielas");
		
		tax.setName("Iva 10%");
		tax.setValue(TEN);
		categoryBooks.addTax(tax);
		categoryLotery.addTax(tax);
		productOther.setCategory(categoryBooks);
		productOther.setCost(ONE_HUNDRED);
		productOther.setMarkup(TEN);// Profit
		
		productLotery.setCategory(categoryLotery);
		productLotery.setCost(ONE_HUNDRED);
		productLotery.setMarkup(TEN);
		//Sale setup
		sale = new Sale("aaa", Long.MAX_VALUE, LocalDate.now());
		// Common saleLine setup
		commonLine = new SaleLine();
		commonLine.setNumber(0);
		commonLine.setProduct(productOther);
		commonLine.setQuantity(1);
		
		//LoteryLine setup
		loteryLine = new SaleLine();
		loteryLine.setNumber(1);
		loteryLine.setProduct(productLotery);
		loteryLine.setQuantity(1);
		
		sale.addLine(commonLine);
		sale.addLine(loteryLine);
		
		sales = new ArrayList<Sale>();
		sales.add(sale);
		
	}

	@Test
	void test() {
		CommonSalesCalculator calculator = new CommonSalesCalculator();
		BigDecimal expected = ONE_HUNDRED_AND_TWENTY_ONE;
		BigDecimal actual = calculator.totalSalesWithOutLotery(sales);
		LOGGER.info("*********** Common sales calculator test *****************");
		LOGGER.info("LArgo de ventas =" +  sales.size());
		LOGGER.info("Expected : " + expected);
		LOGGER.info("Actual : " + actual);
		assertTrue(actual.compareTo(expected) == 0);
	}

}
