package com.controlstock;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.math.BigDecimal;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.controlstock.models.InvoiceLine;


class InvoiceLineTest {

	private static final Logger LOGGER = LogManager.getLogger();
	private static InvoiceLine line;
	private static int QUANTITY = 10;
	private static int COST = 90;
	private static int TAXES = 10;
	@BeforeEach
	void setUp() throws Exception {
		line = new InvoiceLine();
		line.setCosts(new BigDecimal(COST));
		line.setQuantity(QUANTITY);
		line.setTaxes(new BigDecimal(TAXES));
	}

	/**
	 * This test the InvoiceLine method used to calculate total amount for the line.
	 */
	@Test
	void testCalculateTotal() {
		LOGGER.info("********************* Test InvoiceLine *****************");
		
		BigDecimal expected = new BigDecimal(1000);
		BigDecimal actual = line.calculateTotal();
		LOGGER.info("Expected : " + expected);
		LOGGER.info("Actual : " + actual);
		assertTrue(actual.compareTo(expected) == 0);
	}

}
