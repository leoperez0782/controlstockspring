package com.controlstock;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import java.math.BigDecimal;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.controlstock.exceptions.AppException;
import com.controlstock.models.Category;
import com.controlstock.models.Lotery;
import com.controlstock.models.Tax;

class LoteryTest {
	
	static Logger logger = LoggerFactory.getLogger(LoteryTest.class);
	private Lotery lotery;
	private Category category;
	private Tax tax;
	private static final BigDecimal ONE = new BigDecimal(1);
	private static final BigDecimal TEN = new BigDecimal(10);
	

	@BeforeEach
	void setUp() throws Exception {
		lotery = new Lotery();
		category = new Category();
		tax = new Tax();
		tax.setName("this is a useless tax");
		tax.setValue(TEN);
		
		category.setCategoryName("Loteria");
		category.addTax(tax);
		
		lotery.setActive(true);
		lotery.setCategory(category);
		lotery.setCost(ONE);
		lotery.setMarkup(ONE);
	}

	@Test
	void testCalculateProfit_should_return_one() {
		BigDecimal expected = ONE;
		BigDecimal actual = lotery.calculateProfit();
		assertEquals(expected, actual);
	}

	@Test
	void testCalculatePriceWithProfit_should_return_one() {
		BigDecimal expected = ONE;
		BigDecimal actual = lotery.calculatePriceWithProfit();
		assertEquals(expected, actual);
	}

	@Test
	void testCalculateSalePrice_should_return_one() {
		BigDecimal expected = ONE;
		BigDecimal actual;
		try {
			actual = lotery.calculateSalePrice();
			assertEquals(expected, actual);
		} catch (AppException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail("Exception throwed");
		}
		
	}

}
