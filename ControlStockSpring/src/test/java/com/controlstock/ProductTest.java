package com.controlstock;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.math.BigDecimal;

import com.controlstock.exceptions.AppException;
import com.controlstock.models.Category;
import com.controlstock.models.Product;
import com.controlstock.models.Tax;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class ProductTest {

	static Logger logger = LoggerFactory.getLogger(ProductTest.class);
	static Product product;
	static Category category;
	static Tax tax;
	//private static final BigDecimal ONE = new BigDecimal(1);
	private static final BigDecimal TEN = new BigDecimal(10);
	private static final BigDecimal ONE_HUNDRED = new BigDecimal(100);
	private static final BigDecimal ONE_HUNDRED_AND_TEN = new BigDecimal(110);
	private static final BigDecimal ONE_HUNDRED_AND_TWENTY_ONE = new BigDecimal(121);
	private static final BigDecimal ONE_HUNDRED_THOUSAND = new BigDecimal(100000);
	@BeforeEach
	void setUp() throws Exception {
		product = new Product();
		category = new Category();
		tax = new Tax();
		//add attributes
		category.setCategoryName("Libros");
		tax.setName("Iva 10%");
		tax.setValue(TEN);
		category.addTax(tax);
		product.setCategory(category);
		product.setCost(ONE_HUNDRED);
		product.setMarkup(TEN);//Profit
	}

	@Test
	void testCalculateProfit() {
		
		BigDecimal expected = TEN;
		BigDecimal actual = product.calculateProfit();
		assertTrue(actual.compareTo(expected) == 0);
	}
	
	@Test
	void testCalculatePriceWithProfit() {
		
		BigDecimal expected = ONE_HUNDRED_AND_TEN;
		BigDecimal actual = product.calculatePriceWithProfit();
		assertTrue(actual.compareTo(expected) == 0);
	}
	
	@Test
	void testCalculateSalePrice() {
		
		BigDecimal expected = ONE_HUNDRED_AND_TWENTY_ONE;
		BigDecimal actual = ONE_HUNDRED_THOUSAND;
		try {
			actual = product.calculateSalePrice();
		} catch (AppException e) {
			fail("El metodo no deberia recibir un valor negativo en este test");
			e.printStackTrace();
		}
		assertTrue(actual.compareTo(expected) == 0);
	}
	
	@Test
	void testShould_calculateProfit_return_zero_if_markup_is_null() {
		
		product.setMarkup(null);
		assertTrue(product.calculateProfit().equals(BigDecimal.ZERO));
	}
	
	@Test
	void should_calculateProfit_return_zero_if_cost_is_null() {
		
		product.setCost(null);
		assertTrue(product.calculateProfit().equals(BigDecimal.ZERO));
	}
	
	@Test
	void should_calculatePriceWithProfit_return_zero_if_cost_is_null() {
		product.setCost(null);
		assertTrue(product.calculatePriceWithProfit().equals(BigDecimal.ZERO));
	}
	@Test
	void should_calculatePriceWithProfit_return_cost_if_markup_is_null() {
		product.setMarkup(null);
		assertTrue(product.calculatePriceWithProfit().compareTo(product.getCost()) == 0);
	}
	@Test
	void should_calculateSalePrice_return_zero_if_markup_is_null() {
		product.setMarkup(null);
		try {
			assertTrue(product.calculateSalePrice().equals(BigDecimal.ZERO));
		} catch (AppException e) {
			fail("El metodo no deberia recibir un valor negativo en este test");
			e.printStackTrace();
		}
	}
	@Test
	void should_calculateSalePrice_return_zero_if_cost_is_null() {
		product.setCost(null);
		try {
			assertTrue(product.calculateSalePrice().equals(BigDecimal.ZERO));
		} catch (AppException e) {
			fail("El metodo no deberia recibir un valor negativo en este test");
			e.printStackTrace();
		}
	}
	@Test
	void should_not_calculateSalePrice_return_zero_if_cost_and_markup_are_ok() {
		
		try {
			assertFalse(product.calculateSalePrice().equals(BigDecimal.ZERO));
		} catch (AppException e) {
			fail("El metodo no deberia recibir un valor negativo en este test");
			e.printStackTrace();
		}
	}
	
	
}
