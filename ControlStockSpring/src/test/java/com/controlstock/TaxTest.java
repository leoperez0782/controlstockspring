package com.controlstock;

import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.math.BigDecimal;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.controlstock.exceptions.AppException;
import com.controlstock.models.Tax;

class TaxTest {
	private static Tax imp;
	private BigDecimal productPrice = new BigDecimal(100);
	private BigDecimal expectedValue = new BigDecimal(10);

	@BeforeEach
	void setUp() throws Exception {
		imp = new Tax();
		imp.setName("IVA 10%");
		imp.setValue(new BigDecimal(10));
	}

	@Test
	void test() {

		BigDecimal actual = new BigDecimal(10000);
		try {
			actual = imp.calculateValue(productPrice);
		} catch (AppException e) {
			fail("El metodo no deberia recibir un valor negativo en este test");
			e.printStackTrace();
		}

		assertTrue(actual.compareTo(expectedValue) == 0);
	}

	@Test
	void shouldThrowExceptionWithNegativeTaxValue() {
		productPrice = new BigDecimal(-100);

		assertThatExceptionOfType(AppException.class).isThrownBy(() -> {
			imp.calculateValue(productPrice);
		});

	}

}
