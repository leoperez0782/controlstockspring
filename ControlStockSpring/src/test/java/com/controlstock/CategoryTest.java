package com.controlstock;

import static org.junit.jupiter.api.Assertions.*;

import java.math.BigDecimal;

import org.junit.jupiter.api.Test;

import com.controlstock.exceptions.AppException;
import com.controlstock.models.Category;
import com.controlstock.models.Tax;

class CategoryTest {

	@Test
	void test() {
		Category categoria = new Category();
		Tax tax = new Tax();
		tax.setName("Iva 10%");
		tax.setValue(new BigDecimal(10));
		categoria.addTax(tax);
		BigDecimal productPrice = new BigDecimal(100);
		BigDecimal expected = new BigDecimal(10);
		BigDecimal actual = new BigDecimal(10000);
		try {
			actual = categoria.totalTaxesValue(productPrice);
		} catch (AppException e) {
			
			e.printStackTrace();
		}
		
		assertTrue(actual.compareTo(expected) == 0);
	}

}
