package com.controlstock;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.controlstock.exceptions.AppException;
import com.controlstock.models.Category;
import com.controlstock.models.Lotery;
import com.controlstock.models.Product;
import com.controlstock.models.SaleLine;
import com.controlstock.models.Tax;

class SaleLineTest {

	static Product product;
	static Category category;
	static Tax tax;
	static SaleLine line;
	private static final BigDecimal TEN = new BigDecimal(10).setScale(2, RoundingMode.HALF_UP);
	private static final BigDecimal ONE_HUNDRED = new BigDecimal(100).setScale(2, RoundingMode.HALF_UP);
	private static final BigDecimal ONE_HUNDRED_AND_TEN = new BigDecimal(110).setScale(2, RoundingMode.HALF_UP);
	private static final BigDecimal ONE_HUNDRED_AND_TWENTY_ONE = new BigDecimal(121).setScale(2, RoundingMode.HALF_UP);
	private static final BigDecimal ONE_THOUSAND_TWO_HUNDRED_AND_TEN = new BigDecimal(1210).setScale(2, RoundingMode.HALF_UP);
	private static final int ONE_ITEM_QTY = 1;
	private static final int TEN_ITEMS_QTY = 10;

	@BeforeEach
	void setUp() throws Exception {
		// Product setup
		product = new Product();
		category = new Category();
		tax = new Tax();
		// add attributes
		category.setCategoryName("Libros");
		tax.setName("Iva 10%");
		tax.setValue(TEN);
		category.addTax(tax);
		product.setCategory(category);
		product.setCost(ONE_HUNDRED);
		product.setMarkup(TEN);// Profit

		// SaleLine setup
		line = new SaleLine();
		line.setNumber(0);
		line.setProduct(product);
	}

	@Test
	void should_calculateTotal_return_one_hundred_and_twenty_one_if_qty_is_one() {
		line.setQuantity(ONE_ITEM_QTY);
		
		try {
			assertTrue(line.calculateTotal().equals(ONE_HUNDRED_AND_TWENTY_ONE));
		} catch (AppException e) {
			fail("no deberia fallar si el setup se realizo correctamente");
			e.printStackTrace();
		}
		
	}
	@Test
	void should_calculateTotal_return_one_thousand_twohundred_and_ten_if_qty_is_ten() {
		line.setQuantity(TEN_ITEMS_QTY);
		
		try {
			assertTrue(line.calculateTotal().equals(ONE_THOUSAND_TWO_HUNDRED_AND_TEN));
		} catch (AppException e) {
			fail("no deberia fallar si el setup se realizo correctamente");
			e.printStackTrace();
		}
		
	}
	
	@Test
	void should_calculateTotal_update_total_property_when_invoked() {
		line.setQuantity(ONE_ITEM_QTY);
		try {
			BigDecimal expected = line.calculateTotal();
			assertTrue(line.getTotal().equals(expected));
		} catch (AppException e) {
			fail("no deberia fallar si el setup se realizo correctamente");
			e.printStackTrace();
		}
	}
	
	@Test
	void should_getTaxes_return_ten_when_qty_is_one() {
		line.setQuantity(ONE_ITEM_QTY);
		assertTrue(line.getTaxes().equals(TEN));
	}
	
	@Test
	void should_getTotalWithOutTaxes_return_product_price_with_profit_multiply_by_qty_when_invoked() {
		line.setQuantity(ONE_ITEM_QTY);
		assertTrue(line.getTotalWithOutTaxes().equals(ONE_HUNDRED_AND_TEN));
	}
	
	@Test
	void should_return_ten_when_lotery_qty_is_ten() {
		Lotery prod = new Lotery();
		Category cat = new Category();
		cat.setCategoryName("Loteria");
		Tax tax2 = new Tax();
		tax2.setValue(new BigDecimal(1));
		cat.addTax(tax2);
		prod.setCategory(cat);
		line.setQuantity(TEN_ITEMS_QTY);
		line.setProduct(prod);
		try {
			line.calculateTotal();
		} catch (AppException e) {
		
			e.printStackTrace();
		}
		assertTrue(line.getTotal().equals(TEN));
	}

}
