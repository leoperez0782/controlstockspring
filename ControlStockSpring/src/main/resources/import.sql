/*Poulate tables*/
INSERT INTO taxes VALUES (1,'Iva 20', 20.0) ;
INSERT INTO taxes VALUES (2,'Iva 18.03', 18.03) ;
INSERT INTO taxes VALUES (3,'Loteria quiniela y recargas', 1.0) ;

INSERT INTO categories VALUES (1,'Comestibles');
INSERT INTO categories VALUES (2,'Bebidas');
INSERT INTO categories VALUES (3,'Salon');
INSERT INTO categories VALUES (4,'Loterias y quinielas');
INSERT INTO categories VALUES (5,'Recargas');

INSERT INTO categories_taxes VALUES(1,1);
INSERT INTO categories_taxes VALUES(2,2);
INSERT INTO categories_taxes VALUES(3,1);
INSERT INTO categories_taxes VALUES(4,3);
INSERT INTO categories_taxes VALUES(5,3);

INSERT INTO products VALUES (1, 1,'1234', 70, '-', 20, 'alfajor chocolate', 0, 'Vascolet', 1);
INSERT INTO products VALUES (2, 1,'2234', 70, '-', 20, 'alfajor nieve', 0, 'Portezuelo', 1);
INSERT INTO products VALUES (3, 1,'3241', 90, '-', 25, 'Galletitas miel', 0, 'El trigal', 1);
INSERT INTO products VALUES (4, 1,'4234', 90, '-', 15, 'Coca 1.5 L', 0, 'Coca cola', 2);
INSERT INTO products VALUES (5, 1,'3234', 100, '-', 12, 'Jugo Naranja', 0, 'Conaprole', 2);
INSERT INTO products VALUES (6, 1,'325233', 50, '-', 30, 'Lapicera', 0, 'Bic', 3);
INSERT INTO products VALUES (7, 1,'32523', 1, '-', 1, 'Recarga', 0, 'Recarga', 5);
INSERT INTO products VALUES (8, 1,'3252', 1, '-', 1, 'Loteria', 0, 'Loteria', 4);

INSERT INTO lotery VALUES(7);
INSERT INTO lotery VALUES(8);

INSERT INTO users VALUES(1,1,'$2a$10$mW0WSA8PIMGxgt416REzMOKP0iK9FjRdhEy7RjgpbJZR9eWgYHZHa','admin');
INSERT INTO users VALUES(2,1,'$2a$10$mW0WSA8PIMGxgt416REzMOKP0iK9FjRdhEy7RjgpbJZR9eWgYHZHa','leo');

INSERT INTO authorities VALUES(1,'ROLE_ADMIN');
INSERT INTO authorities VALUES(2,'ROLE_USER');

INSERT INTO user_role VALUES(1,1);
INSERT INTO user_role VALUES(1,2);
INSERT INTO user_role VALUES(2,2);

