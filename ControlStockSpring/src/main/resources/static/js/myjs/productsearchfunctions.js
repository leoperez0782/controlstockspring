var products;
const search = document.getElementById("buscar_producto");
const tableBody = document.getElementById("cargarItemProductos");
const cleanButton = document.getElementById('btnLimpiar');
cleanButton.addEventListener("click", (e) =>{
	search.value ="";
	tableBody.innerHTML ="";
});
window.addEventListener("load", function(){
	
    
    search.addEventListener("keyup", function(event){hinter(event)});
    search.addEventListener("blur", (e)=>{
    	addSelectedProduct(e);
    });
    this.window.hinterXHR = new XMLHttpRequest();
});

function hinter(event){
    const input = event.target;

    const list = document.getElementById("lista");

    let min_chars = 0;

    if(input.value.length < min_chars){
        return;
    }else{
        window.hinterXHR.abort();

        window.hinterXHR.onreadystatechange = function(){
            if(this.readyState == 4 && this.status == 200){

                products = JSON.parse( this.responseText );

                list.innerHTML ="";
        
                products.forEach(function(item){
                	
                    let option = document.createElement('option');
                    option.value = item.name + " "+ item.trademark;
                    list.appendChild(option)
                });
            }
        };

       window.hinterXHR.open("GET", "/productos/cargar-productos/" + input.value, true);
       window.hinterXHR.send();

    }
}

function addSelectedProduct(event){
	let name = search.value;
	let product;
	
	products.forEach((item)=>{
		let completeName = item.name + " "+ item.trademark;
		if(completeName === name){
			product = item;
			
		}
	});
	
	loadProductData(product);
}

function loadProductData(product){

	let row = document.createElement('tr');
	let cell1 = document.createElement('td');
	let span1 = document.createElement('span');
	span1.appendChild(document.createTextNode(product.trademark));
	cell1.appendChild(span1);
	
	let cell2 = document.createElement('td');
	let span2 = document.createElement('span');
	span2.appendChild(document.createTextNode(product.name));
	cell2.appendChild(span2);
	
	let cell3 = document.createElement('td');
	let span3 = document.createElement('span');
	span3.appendChild(document.createTextNode(product.stock));
	cell3.appendChild(span3);
	
	let cell4 = document.createElement('td');
	let anchor1 = document.createElement('a');
	anchor1.classList.add("btn");
	anchor1.classList.add("btn-primary");
	anchor1.classList.add("btn-xs");
	anchor1.href = `/productos/editar/${product.id}`;
	anchor1.text = "editar";
	cell4.appendChild(anchor1);
	
	let cell5 = document.createElement('td');
	let anchor2 = document.createElement('a');
	anchor2.classList.add("btn");
	anchor2.classList.add("btn-primary");
	anchor2.classList.add("btn-xs");
	anchor2.href = `/productos/cambiar-categoria/${product.id}`;
	anchor2.text = "cambiar categoría";
	cell5.appendChild(anchor2);
	
	let cell6 = document.createElement('td');
	let anchor3 = document.createElement('a');
	anchor3.classList.add("btn");
	anchor3.classList.add("btn-danger");
	anchor3.classList.add("btn-xs");
	anchor3.href = `/productos/eliminar/${product.id}`;
	anchor3.text = "eliminar";
	//aca activo funcion js para confirm
	anchor3.addEventListener("click", ()=> {
		confirm('estas seguro que quieres eliminar?');
	});
	cell6.appendChild(anchor3);
	
	row.appendChild(cell1);
	row.appendChild(cell2);
	row.appendChild(cell3);
	row.appendChild(cell4);
	row.appendChild(cell5);
	row.appendChild(cell6);
	tableBody.appendChild(row);
				
}