"use strict";

var products;
var cart = [];
const search = document.getElementById("buscar_producto");
const tableBody = document.getElementById("cargarItemProductos");
const paymentAmount = document.getElementById('pagacon');
const devolutionSpan = document.getElementById('vuelto');
const form = document.getElementById('formulario');
const list = document.getElementById('lista');

window.addEventListener("load", function(){
	
    
    search.addEventListener("input", function(event){hinter(event)});
    search.addEventListener("blur", (e)=>{
    	console.log(e);
    	addSelectedProduct(e);
    	search.value ="";
    	calculateReturnAmount();
    });
    this.window.hinterXHR = new XMLHttpRequest();
    
    paymentAmount.addEventListener("input", function(event){
    	calculateReturnAmount();
    });
    form.addEventListener("keypress", (e)=>{
    	
    	if(e.keyCode == 13 || e.wich == 13){
    		e.preventDefault();
    		return false;
    	}
    });
    
    list.addEventListener("select", (event)=>{
    	search.preventDefault();
    	//search.value = list.
    	addSelectedProduct(event);
    });
});

function hinter(event){
    const input = event.target;

    //const list = document.getElementById("lista");

    let min_chars = 0;

    if(input.value.length < min_chars){
        return;
    }else{
        window.hinterXHR.abort();

        window.hinterXHR.onreadystatechange = function(){
            if(this.readyState == 4 && this.status == 200){

                products = JSON.parse( this.responseText );

                list.innerHTML ="";
        
                products.forEach(function(item){
                	
                    let option = document.createElement('option');
                    option.value = item.name + " "+ item.trademark;
                    option.id = item.name;
                    list.appendChild(option)
                });
            }
        };

       window.hinterXHR.open("GET", "/ventas/cargar-productos/" + input.value, true);
       window.hinterXHR.send();

    }
}

function calculateReturnAmount(){
	let payment =parseFloat( paymentAmount.value);
	let total = parseFloat(document.getElementById('gran_total').innerHTML);
	console.log('Pago desde calcular vuelto: ' + payment);
	console.log('Total desde calcular vuelto: ' + total);
	let returnAmount = (payment - total).toFixed(1);
	console.log(returnAmount);
	if(! (isNaN(returnAmount))){
		devolutionSpan.innerHTML = returnAmount;
	}else{
		devolutionSpan.innerHTML ="";
		paymentAmount.value = "";
	}
}
function addSelectedProduct(event){
	let name = search.value;
	console.log("Se busca " + name);
	let product;
	
	products.forEach((item) => {
		let completeName = item.name + " "+ item.trademark;
		if(completeName === name){
			product = item;
			
		}
	});
	
	loadProductData(product);
}

function loadProductData(product){
	if( typeof product !== 'undefined'){
		let found = false;
		let qty = 1;
		let totalPrice = product.salePrice.toFixed(1);
		if(cart.length != 0){
			cart.forEach( (e) => {
				if(e.id === product.id){
					qty++;
					totalPrice = (totalPrice * qty).toFixed(1);
					found =true;
				}
			});
		}
		cart.push(product);
		if(found){
			updateProduct(product, qty);
		}else{
			let row = document.createElement('tr');
			row.id ='row_' + product.id;
			let cell0 = document.createElement('td');
			cell0.classList.add('d-none');
			let num0 = document.createElement('input');
			num0.name = 'item_id[]';
			num0.value = product.id;
			num0.type = 'hidden';
			cell0.appendChild(num0);
			let cell1 = document.createElement('td');
			let span1 = document.createElement('span');
			span1.appendChild(document.createTextNode(product.trademark + " " + product.name));
			cell1.appendChild(span1);
			
			let cell2 = document.createElement('td');
			let span2 = document.createElement('span');
			span2.appendChild(document.createTextNode(product.salePrice.toFixed(1)));
			cell2.appendChild(span2);
			
			let cell3 = document.createElement('td');
			let num3 = document.createElement('input');
			num3.id = 'qty_' + product.id;
			num3.name = 'qty[]';
			num3.type = 'number';
			num3.value = qty;
			num3.min = 1;
			num3.addEventListener("change", (e)=>{
				let price =parseFloat( e.target.parentNode.
				previousElementSibling.childNodes[0].innerHTML);

				let qty =parseFloat( e.target.value);
				
				let prodId = e.target.id.substring(4);
				let spanId = 'total_' + prodId;
				let spanTotal = document.getElementById(spanId);
				updateProductQty(prodId, qty, spanTotal);
				calculateReturnAmount();
			});
			
			cell3.appendChild(num3);
			
			
			let cell5 = document.createElement('td');
			let span5 =document.createElement('span');
			span5.id = 'total_' + product.id;
			span5.appendChild(document.createTextNode(totalPrice));
			cell5.appendChild(span5);
			
			let cell6 = document.createElement('td');
			let anchor3 = document.createElement('a');
			anchor3.classList.add("btn");
			anchor3.classList.add("btn-danger");
			anchor3.classList.add("btn-xs");
			anchor3.href = "#";
			anchor3.text = "eliminar";
			anchor3.id = "del_"+product.id;
			
			anchor3.addEventListener("click", (e)=> {
				let anchor_id = e.target.id;
				let row_id = "row_" + anchor_id.substring(4,);
				let row = document.getElementById(row_id);
				tableBody.deleteRow(row.rowIndex -1);
				let prodId =anchor_id.substring(4,);
				
				removeFromCart(prodId);
				updateGrandTotal();
				calculateReturnAmount();
			});
			cell6.appendChild(anchor3);
			
			row.appendChild(cell0);
			row.appendChild(cell1);
			row.appendChild(cell2);
			row.appendChild(cell3);
			// row.appendChild(cell4);
			row.appendChild(cell5);
			row.appendChild(cell6);
			tableBody.appendChild(row);
			updateGrandTotal();
		}
		
	}
}
	
	function updateProduct(product, qty){
		let selectedRow;
		let rowId = 'row_' + product.id;
		let spanId = 'total_' + product.id;
		tableBody.childNodes.forEach((e) =>{
			if(e.id == rowId){
				selectedRow = e;
			}
		});
		
	
		let total = (product.salePrice * qty).toFixed(1);
		let spanTotal = document.getElementById(spanId);
		spanTotal.innerHTML = total;
		let qtyInput = selectedRow.children[3];
		qtyInput.children[0].value = qty;
		updateGrandTotal();
	}
	
	function updateProductQty(prodId, qty, spanTotal){
		let prod; 
		let actuals = 0;
		cart.forEach((e) =>{
			if(e.id == prodId){
				prod = e;
				actuals++;
			}
		});
		
		if(typeof prod != 'undefined'){
			let diff = qty - actuals;
			if(diff > 0){
				for(var i = 0; i < diff; i++){
					var newProd = Object.assign({}, prod);
					
					cart.push(newProd);
				}
				updateGrandTotal();
			}else{
				let index = 0;
				let indexes =[];
				while(diff != 0){
					if(cart[index].id == prodId){
						indexes.push(index);
						diff++;
					}
					index++;
				}
				for(var i = 0; i < indexes.length; i++){
					cart.splice(indexes[i], 1);
				}
				
			}
			let tot = (prod.salePrice * qty).toFixed(1);
			console.log("total :" + tot);
			console.log(spanTotal);
			spanTotal.innerHTML = tot;
			updateGrandTotal();
		}
		
	}
	
	function updateGrandTotal(){
		
		let total = 0;
		cart.forEach((e) =>{
			total += e.salePrice;
			
		});
		
		document.getElementById('gran_total').innerHTML = total.toFixed(1);
	}
	
	function removeFromCart(prodId){

		cart = cart.filter( p => p.id != prodId);

	}
