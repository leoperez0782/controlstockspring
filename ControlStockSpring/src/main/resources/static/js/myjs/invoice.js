/**
 * 
 */
window.addEventListener("load", function(){
    var search = document.getElementById("buscar_producto");
    search.addEventListener("keyup", function(event){hinter(event)});

    this.window.hinterXHR = new XMLHttpRequest();
});

function hinter(event){
    var input = event.target;

    var list = document.getElementById("lista");

    var min_chars = 0;

    if(input.value.length < min_chars){
        return;
    }else{
        window.hinterXHR.abort();

        window.hinterXHR.onreadystatechange = function(){
            if(this.readyState == 4 && this.status == 200){

                var response = JSON.parse( this.responseText );

                list.innerHTML ="";
        
                response.forEach(function(item){
                    var option = document.createElement('option');
                    option.value = item.name;
                    list.appendChild(option)
                });
            }
        };

       window.hinterXHR.open("GET", "/productos/cargar-productos/" + input.value, true);
       window.hinterXHR.send();

    }
}

