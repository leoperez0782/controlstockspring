/**
 * 
 */
$(document).ready(function() {
	$("#detalleBoleta").hide();
	$("#verProductos").click(function() {
		showProducts();
	});

	$("#verFactura").click(function() {
		showInvoice();
	});
	$("#formulario").keypress(function(e) {
		if (e.which == 13) {
			return false;
		}
	});
	
	$(document).on("click", ".remove", function(event){
		event.preventDefault();
	    $(this).closest('tr').remove();
	    totalCalculate();
	});
	

	function showProducts(){
		$("#datosBoleta").hide();
		$("#detalleBoleta").show();
	}
	function showInvoice(){
		$("#datosBoleta").show();
		$("#detalleBoleta").hide();
	}
	
	const searchInput = document.getElementById('buscar_producto');
	searchInput.addEventListener("blur",(e) =>{
		e.target.value = "";
		let label = document.getElementById('lblInfo');
		label.classList.remove("alert-success");
		label.innerText = "";
	} );
	const txtSeries = document.getElementById('txtSeries');
	txtSeries.addEventListener("blur", (e) => removeDangerClass(e));
	
	const txtNumber = document.getElementById('txtNumber');
	txtNumber.addEventListener("blur", (e) => removeDangerClass(e));
	
	const txtDate = document.getElementById('txtDate');
	txtDate.addEventListener("blur", (e) => removeDangerClass(e));
	
	const qtyInput = document.getElementById('cantidadProduct');
	qtyInput.addEventListener("blur", (e) => removeDangerClass(e));
	
	function removeDangerClass(e){
		if(e.target.value !== ""){
			e.target.classList.remove('alert-danger');
		}
	}	
	$("#buscar_producto").autocomplete({
		source : function(request, response) {
			$.ajax({
				url : "/productos/cargar-productos/" + request.term,
				dataType : "json",
				data : {
					term : request.term
				},
				success : function(data) {
					response($.map(data, function(item) {
						return {
							value : item.name + " " + item.trademark,
							label : item.name + " " + item.trademark,
							name : item.name,
							mark : item.trademark,
							dist : item.id
						};
					}));
				},
			});
		},
		select : function(event, ui) {

			$("#nomProduct").val(ui.item.name);
			$("#marcaProduct").val(ui.item.mark);
			$("#selItem").val(ui.item.dist);
			return false;
		}
	});

	$("#btnAddProd").click(function() {
		
		if(qtyInput.value !== "" && (parseInt(qtyInput.value) > 0)){
			let linea = $("#plantillaItemsFactura").html();

			linea = linea.replace(/{ID}/g, $("#selItem").val());
			linea = linea.replace(/{NOMBRE}/g, $("#nomProduct").val());
			linea = linea.replace(/{MARCA}/g, $("#marcaProduct").val());

			$("#cargarItemProductos tbody").append(linea);
			let cantId = "#cantidad_" + $("#selItem").val();
			let costId = "#costo_" + $("#selItem").val();
			let impId = "#imp_" + $("#selItem").val();
			let totalId = "#total_importe_" + $("#selItem").val();
			let costValue = parseInt($("#costoProduct").val());
			let impValue = parseInt($("#impProduct").val());
			let total = costValue + impValue;
			
			$(cantId).val($("#cantidadProduct").val());
			$(costId).val(costValue);
			$(impId).val(impValue);
			$(totalId).html(total);
			totalCalculate();
			$("#lblInfo").addClass("alert-success");
			$("#lblInfo").html("Se agrego el producto a la factura!");
			showInvoice();
			cleanInputs();
			
		}else{
			qtyInput.classList.add('alert-danger');
			alert('Debe ingresar una cantidad mayor que cero');
		}
		
	});

	
	function cleanInputs(){
		$("#nomProduct").val("");
		$("#marcaProduct").val("");
		$("#costoProduct").val("");
		$("#impProduct").val("");
		$("#cantidadProduct").val("");
	}
	
	function totalCalculate() {
		var total = 0;

		$('span[id^="total_importe_"]').each(function() {
			total += parseInt($(this).html());
		});

		$('#gran_total').html(total);
	}
	
	$("form").submit(function(event){
		if(formIsValid()){
			$("#plantillaItemsFactura").remove();//para limpiar la plantilla.
			return;
		}else{
			event.preventDefault();
			alert("Debe completar los campos señalados");
			showInvoice();
		}
		
	});
	
	function formIsValid(){
		let valid = true;
		if(txtSeries.value === ""){
			txtSeries.classList.add("alert-danger");
			valid = false;
		}
		if(txtNumber.value === ""){
			txtNumber.classList.add("alert-danger");
			valid = false;
		}
		if(txtDate.value === ""){
			txtDate.classList.add("alert-danger");
			valid = false;
		}
		return valid;
	}
	
});