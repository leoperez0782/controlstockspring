package com.controlstock.controllers;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.controlstock.interactors.PDFBoxReportGenerator;
import com.controlstock.interactors.ReportGenerator;
import com.controlstock.interactors.SalesCalculator;
import com.controlstock.models.CashRegister;
import com.controlstock.models.Sale;
import com.controlstock.presenters.CashRegisterPresenter;
import com.controlstock.services.CashRegisterService;
import com.controlstock.services.JPAUserDetailService;
import com.controlstock.services.ReportService;
import com.controlstock.services.SaleService;

@Controller
@RequestMapping(value = "/caja")
@SessionAttributes("cashRegister")
public class CashRegisterController {
	
	private static final String PAGE_TITLE_INDICATOR = "titulo";
	private static Logger logger = LoggerFactory.getLogger(CashRegisterController.class);
	@Autowired
	private CashRegisterService cashRegisterService;
	@Autowired
	private JPAUserDetailService userService;
	@Autowired
	private SaleService saleService;
	@Autowired
	private SalesCalculator salesCalculator;
	@Autowired
	private ReportService reportService;
	@Autowired
	private ReportGenerator reportGenerator;
	
	
	@GetMapping(value= "/abrir")
	public String open(Model model) {
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Optional<CashRegister> cashRegisterOp = findActualCashRegister();
		if(cashRegisterOp.isEmpty()) {
			CashRegister cashRegister = new CashRegister();
			cashRegister.open();
			cashRegister.setDate(LocalDate.now());
			cashRegister.setUser(userService.findByUsername(auth.getName()).get());
			cashRegisterService.save(cashRegister);
			
		}
		
		return "redirect:/ventas/venta";
	}
	
	@GetMapping(value="/cerrar")
	public String close(Model model, RedirectAttributes flash,SessionStatus status) {
		Optional<CashRegister> cashRegisterOp = findActualCashRegister();
		CashRegister cashRegister = cashRegisterOp.get();
		cashRegister.closed();
		cashRegister.setSales(saleService.findAllInTimeLapsus(cashRegister.getOpening(), cashRegister.getClose(), LocalDate.now()));
		cashRegisterService.save(cashRegister);
		status.setComplete();
		flash.addFlashAttribute("success", "caja Cerrada!!!");
		return "redirect:/caja/cierre-de-caja";
	}
	
	private Optional<CashRegister> findActualCashRegister(){
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		return cashRegisterService.findByUserAndDate(auth.getName(), LocalDate.now());
	}
	
	@GetMapping(value="opciones")
	public String options() {
		return "/cash-register/options";
	}
	
	@GetMapping(value="/cierre-de-caja")
	public String registeredSales(Model model, RedirectAttributes flash) {
		model.addAttribute(PAGE_TITLE_INDICATOR, "Cierre de caja");
		try {
			Optional<CashRegister> cashRegisterOp = findActualClosedCashRegister();
			if(cashRegisterOp.isPresent()) {
				CashRegisterPresenter presenter = new CashRegisterPresenter();
				List<Sale> sales =cashRegisterOp.get().getSales();
				
				salesCalculator.calculateSales(presenter, sales);
				presenter.setUserName(cashRegisterOp.get().getUser().getUsername());
				presenter.setDate(cashRegisterOp.get().getDate());
				
				PDFBoxReportGenerator generat = new PDFBoxReportGenerator();
				generat.createPDFReport(presenter);
				model.addAttribute("reporte", presenter);
			}
		}catch(Exception e) {
			logger.info("Error gemnerando cierre de caja:"+ e.getMessage());
			flash.addAttribute("error", "No se ha podido cerrar la caja!!");
			return "redirect:/cash-register/options";
		}
		return "/cash-register/registered-sales-at-close";
	}
	private Optional<CashRegister> findActualClosedCashRegister(){
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		logger.info("Nombrede usuario : " + auth.getName());
		return cashRegisterService.findByUserAndDateAndClosed(auth.getName(), LocalDate.now());
	}
	
}
