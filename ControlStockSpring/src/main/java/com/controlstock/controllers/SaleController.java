package com.controlstock.controllers;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.controlstock.exceptions.AppException;
import com.controlstock.models.Product;
import com.controlstock.models.Sale;
import com.controlstock.models.SaleLine;
import com.controlstock.presenters.SalePresenter;
import com.controlstock.presenters.SalePresenterFactory;
import com.controlstock.services.CashRegisterService;
import com.controlstock.services.ProductService;
import com.controlstock.services.SaleService;
@Secured({"ROLE_ADMIN", "ROLE_USER"})
@Controller
@RequestMapping(value = "/ventas")
@SessionAttributes({"sale"})
public class SaleController {

	private static Logger logger = LoggerFactory.getLogger(SaleController.class);
	private static final String PAGE_TITLE_INDICATOR = "titulo";
	private static final String SALE_PAGE_TITLE = "Punto de venta";
	@Autowired
	private SaleService saleService;
	@Autowired
	private SalePresenterFactory presenterFactory;
	@Autowired
	private ProductService productService;
	@Autowired
	private CashRegisterService cashRegisterService;
	
	@GetMapping(value = "/venta")
	public String sale(Model model) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		model.addAttribute(PAGE_TITLE_INDICATOR,SALE_PAGE_TITLE );
		model.addAttribute("sale", new Sale());
		
		if(cashRegisterService.findByUserAndDate(auth.getName(), LocalDate.now()).isPresent()) {
			model.addAttribute("cashRegister", cashRegisterService.findByUserAndDate(auth.getName(), LocalDate.now()).get());
		}
		return "sale/pos";
	}
	
	@GetMapping(value = "/cargar-productos/{term}", produces = { "application/json" })
	public @ResponseBody List<SalePresenter> findByname(@PathVariable String term) {
		
		 return presenterFactory.createFromList( productService.findByNameLikeIgnoreCase(term));
		
	}
	@GetMapping(value = "/buscar-marca/{term}", produces = {"application/json"})
	public @ResponseBody List<SalePresenter> findByTradeMark(@PathVariable String term){
		return presenterFactory.createFromList(productService.findByTrademark(term));
	}
	@GetMapping(value= "/buscar-codigo/{term}", produces = { "application/json" })
	public @ResponseBody List<SalePresenter> findByCode(@PathVariable String term) {
		Optional<Product> prodOp = productService.findByBarCode(term);
		List<SalePresenter> lista = new ArrayList<>();
		if(prodOp.isPresent()) {
			try {
				
				lista.add(presenterFactory.createFromProduct(prodOp.get()));
				
			} catch (AppException e) {
				
				e.printStackTrace();
			}
		}
		return lista;
	}
	
	@PostMapping(value="/guardar")
	public String save(@Valid Sale sale, BindingResult result,Model model,
			@RequestParam(name = "item_id[]", required = false) Long[] itemId,
			@RequestParam(name = "qty[]", required = false) Integer[] quantity,
			RedirectAttributes flash,
			SessionStatus status) {
		if(result.hasErrors()) {
			model.addAttribute(PAGE_TITLE_INDICATOR, "Punto de venta");
			return "sale/sale";
		}
		if(itemId==null || itemId.length == 0) {
			model.addAttribute(PAGE_TITLE_INDICATOR, "Punto de venta");
			model.addAttribute("error", "Error: la factura no puede estar vacia!!");
			return "sale/sale";
		}
		try {
			loadProducts(sale, itemId, quantity);
			saleService.save(sale);
			status.setComplete();
			flash.addFlashAttribute("success", "factura guardada!!!");
		}catch(Exception e) {
			logger.info(String.format("Error desde controller: %s", e.getMessage()));
			e.printStackTrace();
			flash.addFlashAttribute("error", "No se pudo agregar la factura!");
		}
		return "redirect:/ventas/venta";
	}

	private void loadProducts(Sale sale, Long[] itemId, Integer[] quantity) {

		for(int i=0; i< itemId.length; i++) {
			Optional<Product> productOP = productService.findById(itemId[i]);
			if(productOP.isPresent()) {
				SaleLine line = new SaleLine();
				line.setNumber(i);
				line.setProduct(productOP.get());
				line.setQuantity(quantity[i]);
				//update product qty
				productOP.get().substractStock(quantity[i]);
				sale.addLine(line);
			}
		}
		
	}
}
