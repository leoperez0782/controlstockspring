package com.controlstock.controllers;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.controlstock.models.Product;
import com.controlstock.services.CategoryService;
import com.controlstock.services.ProductService;

@Controller
@RequestMapping(value = "/productos")
@SessionAttributes("product")
public class ProductController {

	private static Logger logger = LoggerFactory.getLogger(ProductController.class);
	private static final String PAGE_TITLE_INDICATOR = "titulo";
	@Autowired
	private ProductService productService;
	@Autowired
	private CategoryService categoryService;
	
	@Secured("ROLE_ADMIN")
	@GetMapping(value = "/alta")
	public String create(Model model) {
		model.addAttribute(PAGE_TITLE_INDICATOR, "Agregar producto");
		model.addAttribute("product", new Product());
		model.addAttribute("categories", categoryService.findAll());
		return "/product/create";
	}

	@PostMapping(value = "/guardar")
	public String save(@Valid Product product, BindingResult result, Model model, RedirectAttributes flash,
			SessionStatus status) {
		if (result.hasErrors()) {
			return "/product/create";
		}
		try {
			productService.save(product);
			status.setComplete();
			flash.addFlashAttribute("success", "Producto agregado con éxito!");

		} catch (Exception e) {
			logger.info("Desde controller" + e.getMessage() + e.toString());
			flash.addFlashAttribute("error", "No se pudo agregar el producto!");
		}
		return "redirect:/productos/alta";
	}

	@GetMapping(value = "/cargar-productos/{term}", produces = { "application/json" })
	public @ResponseBody List<Product> findByname(@PathVariable String term) {
		List<Product> lista = productService.findByNameLikeIgnoreCase(term);
		logger.info("Mostrando largo lista productos: " + lista.size());
		return lista;
	}

	@Secured("ROLE_ADMIN")
	@GetMapping("/listar")
	public String listAll(Model model) {
		model.addAttribute(PAGE_TITLE_INDICATOR, "Listado de productos");
		model.addAttribute("products", productService.findAll());
		return "/product/list";
	}

	@Secured("ROLE_ADMIN")
	@GetMapping("/editar/{id}")
	public String editForm(@PathVariable(value = "id") Long id, Model model, RedirectAttributes flash) {
		
		Optional<Product> productOP = productService.findById(id);
		if (productOP.isPresent()) {
			model.addAttribute(PAGE_TITLE_INDICATOR, "Editar producto");
			model.addAttribute("product", productOP.get());
			return "product/edit";
		}else {
			flash.addFlashAttribute("error", "No se encuentra registrado el producto!");
			return "redirect:/productos/listar";
		}
		
	}
	
	@Secured("ROLE_ADMIN")
	@PostMapping("/guardar-cambios")
	public String saveChanges(@Valid Product product, BindingResult result, Model model, RedirectAttributes flash,
			SessionStatus status) {
		if (result.hasErrors()) {
			return "product/edit";
		}
		Optional<Product> productOP = productService.findById(product.getId());
		if (productOP.isPresent()) {
			updateProductData(product, productOP.get());
			try {
				productService.save(product);
				status.setComplete();
				flash.addFlashAttribute("success", "Producto modificado con éxito!");

			} catch (Exception e) {
				logger.info("Desde controller" + e.getMessage() + e.toString());
				flash.addFlashAttribute("error", "No se pudo modificar el producto!");
			}
			return "redirect:/productos/listar";
		}else {
			flash.addFlashAttribute("error", "No se encuentra registrado el producto!");
			return "redirect:/productos/listar";
		}
	}

	private void updateProductData(@Valid Product updated, Product toUpdate) {
		toUpdate.setBarCode(updated.getBarCode());
		toUpdate.setCost(updated.getCost());
		toUpdate.setDescription(updated.getDescription());
		toUpdate.setMarkup(updated.getMarkup());
		toUpdate.setName(updated.getName());
		toUpdate.setTrademark(updated.getTrademark());
		toUpdate.setCategory(updated.getCategory());

	}
	
	@Secured("ROLE_ADMIN")
	@GetMapping("/cambiar-categoria/{id}")
	public String changeCategory(@PathVariable(value = "id") Long id, Model model, RedirectAttributes flash) {
		Optional<Product> productOP = productService.findById(id);
		
		if (productOP.isPresent()) {
			model.addAttribute(PAGE_TITLE_INDICATOR, "Cambiar categoría");
			model.addAttribute("product", productOP.get());
			model.addAttribute("categories", categoryService.findAll());
			return "product/change-category";
		}else {
			flash.addFlashAttribute("error", "No se encuentra registrado el producto!");
			return "redirect:/productos/listar";
		}
		
	}
	
	@Secured("ROLE_ADMIN")
	@GetMapping("/buscar")
	public String search(Model model) {
		model.addAttribute(PAGE_TITLE_INDICATOR, "Buscar productos");
		return "product/search";
	}
	
	@Secured("ROLE_ADMIN")
	@GetMapping("/eliminar/{id}")
	public String delete(@PathVariable(value = "id") Long id, Model model, RedirectAttributes flash) {
		Optional<Product> productOP = productService.findById(id);
		if(productOP.isPresent()) {
			try {
				productOP.get().setActive(false);
				productService.save(productOP.get());
				flash.addFlashAttribute("success", "Producto eliminado con éxito!");
			}catch(Exception e) {
				logger.info("Desde controller" + e.getMessage() + e.toString());
				flash.addFlashAttribute("error", "No se pudo eliminar el producto!");
			}
		}else {
			flash.addFlashAttribute("error", "No se encuentra registrado el producto!");
		}
		return "redirect:/productos/listar";
	}
}
