package com.controlstock.controllers;

import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.controlstock.models.Tax;
import com.controlstock.services.TaxService;
@Secured("ROLE_ADMIN")
@Controller
@RequestMapping(value = "/impuestos")
@SessionAttributes("tax")
public class TaxController {

	private static Logger logger = LoggerFactory.getLogger(TaxController.class);
	private static final String PAGE_TITLE_INDICATOR = "titulo";
	private static final String FORM_BUTTON_TEXT = "buttonText";
	@Autowired
	private TaxService taxService;
	@GetMapping(value="/alta")
	public String create(Model model) {
		model.addAttribute(PAGE_TITLE_INDICATOR, "Alta de impuestos");
		model.addAttribute("tax", new Tax());
		model.addAttribute(FORM_BUTTON_TEXT, "Crear impuesto");
		return "taxes/create";
	}
	
	@PostMapping(value="/guardar")
	public String save(@Valid Tax tax, BindingResult result, Model model, RedirectAttributes flash,
			SessionStatus status) {
		if(result.hasErrors()) {
			return "taxes/create";
		}
		try {
			taxService.save(tax);
			status.setComplete();
			flash.addAttribute("success", "Impuesto agregado con exito!!");
			return "redirect:/impuestos/listar";
		}catch(Exception e) {
			logger.info(String.format("Error desde controller: %s %s", e.getMessage(), e.getMessage()));
			flash.addFlashAttribute("error", "No se pudo agregar el impuesto");
			return "redirect:/impuestos/alta";
		}
		
	}
	
	@GetMapping(value="/listar")
	public String listAll(Model model) {
		model.addAttribute(PAGE_TITLE_INDICATOR, "Listado de impuestos");
		
		model.addAttribute("taxes",taxService.findAll());
		return "taxes/list";
	}
	
	@GetMapping(value="/editar/{id}")
	public String editForm(@PathVariable(value = "id") Long id, Model model, RedirectAttributes flash) {
		Optional<Tax> taxOp = taxService.findById(id);
		if(taxOp.isPresent()) {
			model.addAttribute(PAGE_TITLE_INDICATOR, "Editar impuesto");
			model.addAttribute("tax", taxOp.get());
			model.addAttribute(FORM_BUTTON_TEXT, "Editar");
			return "taxes/create";
		}else {
			flash.addFlashAttribute("error", "No se encuentra registrado el impuesto!");
			return "redirect:/impuestos/listar";
		}
		
	}
	
}
