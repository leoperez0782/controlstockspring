package com.controlstock.controllers;

import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.controlstock.models.Category;
import com.controlstock.services.CategoryService;
import com.controlstock.services.TaxService;

@Controller
@RequestMapping(value="/categorias")
@SessionAttributes("category")
public class CategoryController {
	private static Logger logger = LoggerFactory.getLogger(CategoryController.class);
	private static final String PAGE_TITLE_INDICATOR = "titulo";
	@Autowired
	private CategoryService categoryService;
	@Autowired
	private TaxService taxService;

	@Secured("ROLE_ADMIN")
	@GetMapping(value = "/alta")
	public String create(Model model) {
		model.addAttribute(PAGE_TITLE_INDICATOR, "Alta de categorias");

		model.addAttribute("category", new Category());
		model.addAttribute("impuestos", taxService.findAll());
		return "category/create";
	}
	
	@Secured("ROLE_ADMIN")
	@PostMapping(value="/guardar")
	public String save(@Valid Category category, BindingResult result, Model model, RedirectAttributes flash,
			SessionStatus status) {
		if(result.hasErrors()) {
			return "category/create";
		}
		try {
			categoryService.create(category);
			status.setComplete();
			flash.addFlashAttribute("success", "Categoria agregada con éxito!");

			return "redirect:/categorias/listar";
		}catch(Exception e) {
			logger.info("Desde controller" + e.getMessage() + e.toString());
			flash.addFlashAttribute("error", "No se pudo agregar la categoria!");
			return "redirect:/categorias/alta";
		}
		
	}
	
	@Secured("ROLE_ADMIN")
	@GetMapping(value="/listar")
	public String listAll(Model model) {
		model.addAttribute(PAGE_TITLE_INDICATOR, "Listado de categorias");
		model.addAttribute("categories", categoryService.findAll());
		
		return "category/list";
	}
	
	@Secured("ROLE_ADMIN")
	@GetMapping(value="/editar/{id}")
	public String editForm(@PathVariable(value = "id") Long id, Model model, RedirectAttributes flash) {
		Optional<Category> categoryOp = categoryService.findById(id);
		if(categoryOp.isPresent()) {
			model.addAttribute(PAGE_TITLE_INDICATOR, "Editar categoria");
			model.addAttribute("category", categoryOp.get());
			model.addAttribute("impuestos", taxService.findAll());
			return "category/edit";
		}else {
			flash.addFlashAttribute("error", "No se encuentra la categoria!");
			return "redirect:/categorias/alta";
		}
		
	}
}
