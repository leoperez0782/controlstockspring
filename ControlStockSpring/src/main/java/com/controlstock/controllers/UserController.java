package com.controlstock.controllers;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.controlstock.models.AppUser;
import com.controlstock.presenters.UserPresenter;
import com.controlstock.services.JPAUserDetailService;


@Controller
@Secured("ROLE_ADMIN")
@RequestMapping("/usuario")
@SessionAttributes("user")
public class UserController {

	private static Logger logger = LoggerFactory.getLogger(UserController.class);
	private static final String PAGE_TITLE_INDICATOR = "titulo";
	private static final String CREATE_USER_PAGE_REDIRECT = "redirect:/usuario/alta";
	@Autowired
	private JPAUserDetailService userService;
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;
	
	@GetMapping(value = "/alta")
	public String create(Model model) {
		model.addAttribute(PAGE_TITLE_INDICATOR, "Alta de usuarios");
		model.addAttribute("roles", userService.findAll());
		model.addAttribute("user", new UserPresenter());
		return "/user/create";
	}
	
	@PostMapping(value = "/guardar")
	public String save(@Valid UserPresenter user, BindingResult result, Model model, RedirectAttributes flash,
			SessionStatus status) {
		if(result.hasErrors()) {
			
			return "/user/create";
		}
		try {
			
			AppUser appu =user.createUser();
			appu.setPassword(passwordEncoder.encode(user.getPassword()));
			userService.save(appu);
			status.setComplete();
			flash.addAttribute("success", "Usuario agregado con exito!!");
			return CREATE_USER_PAGE_REDIRECT;
		}catch(DataIntegrityViolationException de) {
			
			logger.info(String.format("Error desde controller: %s %s", de.getMessage(), de.getMessage()));
			flash.addFlashAttribute("error", "Ya existe un usuario con ese nombre");
			return CREATE_USER_PAGE_REDIRECT;
		}
		catch(Exception e) {
			logger.info(String.format("Tipo de la excepion %s", e.getClass()));
			logger.info(String.format("Error desde controller: %s %s", e.getMessage(), e.getMessage()));
			flash.addFlashAttribute("error", "No se pudo agregar el usuario");
			return CREATE_USER_PAGE_REDIRECT;
		}
		
	}
	
	@GetMapping(value="/listar")
	public String list(Model model) {
		model.addAttribute(PAGE_TITLE_INDICATOR, "Listado de usuarios");
		model.addAttribute("users", UserPresenter.loadAppUsers(userService.findAllUsers()));
		return "/user/list";
	}
}
