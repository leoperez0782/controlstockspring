package com.controlstock.controllers;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.controlstock.presenters.ReportRequestPresenter;

@Secured({"ROLE_ADMIN", "ROLE_USER"})
@Controller
@RequestMapping(value = "/reportes")
public class ReportsController {
	
	private static Logger logger = LoggerFactory.getLogger(ReportsController.class);
	private static final String PAGE_TITLE_INDICATOR = "titulo";
	
	@GetMapping(value = "/reportes")
	public String reports(Model model) {
		model.addAttribute(PAGE_TITLE_INDICATOR, "Generación de reportes");
		model.addAttribute("presenter", new ReportRequestPresenter());
		return "/reports/reports";
	}
	
	@PostMapping(value = "/generar-entre-fechas")
	public String reportBetweenDates(@Valid ReportRequestPresenter presenter, BindingResult result,Model model,
			RedirectAttributes flash) {
		
		return "";
	}
}
