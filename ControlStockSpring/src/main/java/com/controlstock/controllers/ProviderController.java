package com.controlstock.controllers;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.controlstock.models.Provider;
import com.controlstock.services.ProviderService;

@Secured("ROLE_ADMIN")
@Controller
@SessionAttributes("provider")
public class ProviderController {

	private static Logger logger = LoggerFactory.getLogger(ProviderController.class);
	private static final String PAGE_TITLE_INDICATOR = "titulo";
	@Autowired
	private ProviderService providerService;
	
	@GetMapping(value = "/proveedor/alta")
	public String create(Model model) {

		model.addAttribute("provider", new Provider());
		model.addAttribute(PAGE_TITLE_INDICATOR, "Alta de proveedor");
		return "provider/create";
	}

	@PostMapping(value = "/proveedor/guardar")
	public String save(@Valid Provider provider, BindingResult result, Model model, RedirectAttributes flash,
			SessionStatus status) {
		model.addAttribute(PAGE_TITLE_INDICATOR, "Alta de proveedores");// queda afuera por que redireccionan todos a la
																		// misma pagina
		if (result.hasErrors()) {

			return "provider/create";
		}
		try {
			providerService.create(provider);
			status.setComplete();
			flash.addFlashAttribute("success", "Proveedor agregado con éxito!");

			return "redirect:/proveedor/listar";

		} catch (Exception e) {

			logger.info("Desde controller" + e.getMessage() + e.toString());
			flash.addFlashAttribute("error", "No se pudo agregar el proveedor! Verifique  rut y razon social");
			return "redirect:/proveedor/alta";
		}

	}

	@GetMapping(value = "/proveedor/listar")
	public String listAll(@RequestParam(name = "page", defaultValue = "0") int page, Model model) {
		model.addAttribute(PAGE_TITLE_INDICATOR, "Listado de proveedores");

		model.addAttribute("providers", providerService.findAll(PageRequest.of(page, 5)));

		return "/provider/list";
	}
}
