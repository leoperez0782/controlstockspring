package com.controlstock.controllers;

import java.math.BigDecimal;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.controlstock.models.Invoice;
import com.controlstock.models.InvoiceLine;
import com.controlstock.models.Product;
import com.controlstock.models.Provider;
import com.controlstock.services.InvoiceService;
import com.controlstock.services.ProductService;
import com.controlstock.services.ProviderService;
@Secured("ROLE_ADMIN")
@Controller
@RequestMapping("/facturas")
@SessionAttributes("invoice")
public class InvoiceController {

	private static Logger logger = LoggerFactory.getLogger(InvoiceController.class);
	private static final String PAGE_TITLE_INDICATOR = "titulo";
	private static final String INVOICES_CREATE_URL = "/invoices/create";
	private static final String ERROR_ATTRIBUTE = "error";
	@Autowired
	private InvoiceService invoiceService;
	@Autowired
	private ProviderService providerService;
	@Autowired
	private ProductService productService;


	@GetMapping(value = "/alta/{id}")
	public String create(@PathVariable(value = "id") Long id, Model model, RedirectAttributes flash) {
		model.addAttribute(PAGE_TITLE_INDICATOR, "Ingreso de facturas");
		Optional<Provider> providerOP = providerService.findById(id);
		if (providerOP.isPresent()) {

			model.addAttribute("invoice", new Invoice(providerOP.get()));
		} else {
			flash.addAttribute(ERROR_ATTRIBUTE, "Error: Proveedor no registrado!");
			return "redirect:/proveedor/listar";
		}

		return INVOICES_CREATE_URL;
	}

	@PostMapping(value = "/guardar")
	public String save(@Valid Invoice invoice, BindingResult result, Model model,
			@RequestParam(name = "item_id[]", required = false) Long[] itemId,
			@RequestParam(name = "cantidad[]", required = false) Integer[] cantidad,
			@RequestParam(name = "costo[]", required = false) BigDecimal[] costs,
			@RequestParam(name = "imp[]", required = false) BigDecimal[] imps, RedirectAttributes flash,
			SessionStatus status) {
		model.addAttribute(PAGE_TITLE_INDICATOR, "Ingreso de facturas");
		if (result.hasErrors()) {

			return INVOICES_CREATE_URL;
		}
		if (itemId == null || itemId.length == 0) {
			model.addAttribute(ERROR_ATTRIBUTE, "Error: la factura no puede estar vacia!!");
			return INVOICES_CREATE_URL;
		}
		try {
			Optional<Provider> providerOP = providerService.findById(invoice.getProvider().getId());
			
			loadInvoice(invoice, itemId, cantidad, costs, imps, providerOP);
			
			invoice.setProvider(providerOP.get());
			invoiceService.create(invoice);
			status.setComplete();
			flash.addFlashAttribute("success", "factura creada con exito!!!");

		} catch (Exception e) {
			
			logger.info(String.format("Error desde controller: %s %s", e.getMessage(), e.getMessage()));
			flash.addFlashAttribute(ERROR_ATTRIBUTE, "No se pudo agregar la factura!");
		}
		return "redirect:/proveedor/listar";
	}

	private void loadInvoice(Invoice invoice, Long[] itemId, Integer[] cantidad, BigDecimal[] costs, BigDecimal[] imps,
			Optional<Provider> providerOP) {
		for (int i = 0; i < itemId.length; i++) {
			Optional<Product> productOp = productService.findById(itemId[i]);
			if(productOp.isPresent()) {
			InvoiceLine line = new InvoiceLine();
			line.setNumber(i);
			line.setProduct(productOp.get());
			line.setQuantity(cantidad[i]);
			line.setCosts(costs[i]);
			line.setTaxes(imps[i]);

			invoice.addLine(line);
			productOp.get().addStock(line.getQuantity());
			providerOP.get().addProduct(productOp.get());
			productOp.get().addProvider(providerOP.get());
			}
		}
	}

}
