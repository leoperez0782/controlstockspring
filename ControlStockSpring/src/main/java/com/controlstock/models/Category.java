package com.controlstock.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.controlstock.exceptions.AppException;
/**
 * 
 * @author leo
 * 10 mar. 2020
 */

@Entity
@Table(name ="categories")
public class Category implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	//@OneToMany(fetch=FetchType.LAZY, cascade= CascadeType.ALL)
	//@JoinColumn(name="category_id")
	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<Tax> taxes = new ArrayList<>();
	private String categoryName;

	
	public Category() {
		super();
	}


	public Category(List<Tax> taxes, String categoryName) {
		super();
		this.taxes = taxes;
		this.categoryName = categoryName;
	}

	

	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public List<Tax> getTaxes() {
		return taxes;
	}


	public void setTaxes(List<Tax> taxes) {
		this.taxes = taxes;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	public String getCategoryName() {
		return categoryName;
	}


	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}


	public void addTax(Tax tax) {
		if(!this.taxes.contains(tax)) {
			this.taxes.add(tax);
		}
	}
	public BigDecimal totalTaxesValue(BigDecimal productPrice) throws AppException {
		BigDecimal total = new BigDecimal(0);
		for(Tax temp : this.taxes) {
			
			total = total.add(temp.calculateValue(productPrice));
		}
		return total;
	}

	
}
