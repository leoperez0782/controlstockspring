package com.controlstock.models;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.PrePersist;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import com.controlstock.exceptions.AppException;

@Entity
@Table(name="sale_lines")
@PrimaryKeyJoinColumn(name ="line_id")
public class SaleLine extends Line{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal totalWithOutTaxes;
	private BigDecimal taxes;
	private BigDecimal total;
	
	
	
	public SaleLine() {
		super();
		
	}

	public SaleLine(Product product, int quantity, int number) {
		super(product, quantity, number);
		
	}

	@PrePersist
	public void prePersist() {
		this.calculateTaxes();
		this.calculatTotalWithOutTaxes();
		try {
			this.calculateTotal();
		} catch (AppException e) {
			
			e.printStackTrace();
		}
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public BigDecimal getTotalWithOutTaxes() {
		calculatTotalWithOutTaxes();
		return totalWithOutTaxes;
	}

	private void calculatTotalWithOutTaxes() {
		this.totalWithOutTaxes = (this.getProduct()
				.calculatePriceWithProfit()
				.multiply(new BigDecimal(this.getQuantity()))).setScale(2,RoundingMode.HALF_UP);
	}

	public void setTotalWithOutTaxes(BigDecimal totalWithOutTaxes) {
		this.totalWithOutTaxes = totalWithOutTaxes;
	}


	public BigDecimal getTaxes() {
		calculateTaxes();
		return this.taxes;
	}


	public void calculateTaxes() {
		List<Tax> tempTaxes = this.getProduct().getCategory().getTaxes();
		this.taxes = new BigDecimal(0);

		tempTaxes.forEach(item -> {
			this.taxes = this.taxes.add(item.getValue());
		});
		
		this.taxes = (taxes.multiply(new BigDecimal(this.getQuantity()))).setScale(2,RoundingMode.HALF_UP);
	}


	public void setTaxes(BigDecimal taxes) {
		this.taxes = taxes;
	}


	public BigDecimal getTotal() {
		return total;
	}


	public void setTotal(BigDecimal total) {
		this.total = total;
	}


	@Override
	public BigDecimal calculateTotal() throws AppException {
		this.total = (this.getProduct().calculateSalePrice().multiply(new BigDecimal(this.getQuantity()))).setScale(2, RoundingMode.HALF_UP);
		return this.total;
	}

}
