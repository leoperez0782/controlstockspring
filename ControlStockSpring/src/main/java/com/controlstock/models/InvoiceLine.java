package com.controlstock.models;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
/**
 * 
 * @author Leonardo Pérez
 * 11 mar. 2020
 */
@Entity
@Table(name="invoice_lines")
@PrimaryKeyJoinColumn(name ="line_id")
public class InvoiceLine extends Line{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal costs;
	private BigDecimal taxes;
	
	
	public InvoiceLine() {
		super();

	}


	public InvoiceLine(Product product, int quantity, int number, BigDecimal costs, BigDecimal taxes) {
		super(product, quantity, number);
		this.costs = costs;
		this.taxes = taxes;
	}

	

	public BigDecimal getCosts() {
		return costs;
	}


	public void setCosts(BigDecimal costs) {
		this.costs = costs;
	}


	public BigDecimal getTaxes() {
		return taxes;
	}


	public void setTaxes(BigDecimal taxes) {
		this.taxes = taxes;
	}


	@Override
	public BigDecimal calculateTotal() {
		BigDecimal qty = new BigDecimal(this.getQuantity());
		return (this.costs.add(this.taxes)).multiply(qty);
	}
}
