package com.controlstock.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

import org.hibernate.annotations.NaturalId;

/**
 * 
 * @author Leonardo Pérez 8 mar. 2020
 */
@Entity
@Table(name = "providers")
public class Provider implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Pattern(regexp = "\\d{12}", message = "El rut debe tener 12 digitos")
	@NaturalId
	@Column(unique = true)
	private String rut;
	@NotEmpty
	private String name;
	private String address;
	@NotEmpty
	@NaturalId
	@Column(unique = true)
	private String registeredName;
	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(name = "provider_product",
	joinColumns = @JoinColumn(name = "provider_id"),
	inverseJoinColumns = @JoinColumn(name = "product_id"))
	private List<Product> products = new ArrayList<>();
	// private ArrayList<Contact> sellers = new ArrayList<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRut() {
		return rut;
	}

	public void setRut(String rut) {
		this.rut = rut;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getRegisteredName() {
		return registeredName;
	}

	public void setRegisteredName(String registeredName) {
		this.registeredName = registeredName;
	}

	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}

//	public ArrayList<Contact> getSellers() {
//		return sellers;
//	}
//	public void setSellers(ArrayList<Contact> sellers) {
//		this.sellers = sellers;
//	}
//    
	public void addProduct(Product prod) {
		this.products.add(prod);
	}
}
