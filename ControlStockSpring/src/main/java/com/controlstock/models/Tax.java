package com.controlstock.models;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.controlstock.exceptions.AppException;
/**
 * 
 * @author Leonardo Pérez
 * 11 mar. 2020
 */
@Entity
@Table(name="taxes")
public class Tax implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@NotNull
	private BigDecimal value;
	@NotEmpty
	private String name;
	
	
	public Tax() {
		super();
	}


	public Tax(BigDecimal valor, String nombre) {
		super();
		this.value = valor;
		this.name = nombre;
	}

	

	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	public BigDecimal getValue() {
		return value;
	}


	public void setValue(BigDecimal valor) {
		this.value = valor;
	}


	public String getName() {
		return name;
	}


	public void setName(String nombre) {
		this.name = nombre;
	}


	public BigDecimal calculateValue(BigDecimal productPrice) throws AppException {
		if(productPrice.compareTo(BigDecimal.ZERO) <= 0) {
			throw new AppException("El valor del impuesto no puede ser negativo");
		}
		return (productPrice.multiply(this.value)).divide(new BigDecimal(100));
	}
	
	
	
}
