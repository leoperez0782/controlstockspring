package com.controlstock.models;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;

@Entity
@Table(name = "cash_register")
public class CashRegister implements Serializable {
	public enum Shift {MATUTINO, VESPERTINO, NOCTURNO}
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private LocalDate date;
	private LocalTime opening;
	private LocalTime close;
	@OneToMany(fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
	private List<Sale> sales = new ArrayList<>();
	@OneToOne
	private AppUser user;
	@Enumerated(EnumType.STRING)
	
	private Shift shift; 
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public LocalTime getOpening() {
		return opening;
	}

	

	public LocalTime getClose() {
		return close;
	}

	

	public List<Sale> getSales() {
		return sales;
	}

	public void setSales(List<Sale> sales) {
		this.sales = sales;
	}

	
	public AppUser getUser() {
		return user;
	}

	public void setUser(AppUser user) {
		this.user = user;
	}

	public void open() {
		if (!isOpen()) {
			this.opening = LocalTime.now();
		}
	}
	public boolean isOpen() {
		return this.opening != null;
	}
	
	public boolean isClosed() {
		return this.close != null;
	}
	
	public void closed() {
		if(!isClosed()) {
			this.close = LocalTime.now();
		}
	}
	
	public void addSale(Sale sale) {
		this.sales.add(sale);
	}

	@PrePersist
	public void resolveShift() {
		int hour = this.opening.getHour();
		if(hour > 6 && hour < 15) {
			this.shift = Shift.MATUTINO;
		}else if(hour > 14 && hour < 22) {
			this.shift = Shift.VESPERTINO;
		}else {
			this.shift = Shift.NOCTURNO;
		}
	}
}
