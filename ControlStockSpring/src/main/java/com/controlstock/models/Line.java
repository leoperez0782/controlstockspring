package com.controlstock.models;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.controlstock.exceptions.AppException;
/**
 * 
 * @author Leonardo Pérez
 * 13 mar. 2020
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Line implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@OneToOne(fetch=FetchType.LAZY, cascade= CascadeType.ALL)
	@JoinColumn(name="procuct_id")
	private Product product;
	private int quantity;
	private int number;

	public Line() {
		super();
		
	}

	public Line(Product product, int quantity, int number) {
		super();
		this.product = product;
		this.quantity = quantity;
		this.number = number;
	}

	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public abstract BigDecimal calculateTotal() throws AppException;

}
