package com.controlstock.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.controlstock.exceptions.AppException;

/**
 * 
 * @author Leonardo Pérez 11 mar. 2020
 */
@Entity
@Table(name="products")
@Inheritance(strategy = InheritanceType.JOINED)
public class Product implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@NotEmpty
	private String name;
	@NotEmpty
	private String trademark;
	
	private BigDecimal cost;
	@NotEmpty
	private String barCode;
	private String description;
	
	private BigDecimal markup;
	
	private int stock;
	@NotNull
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="category_id")
	private Category category;
	@ManyToMany(mappedBy = "products", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<Provider> providers = new ArrayList<>();
	private boolean active;
	
	public Product() {
		super();
		
	}

	public Product(String name, String trademark, BigDecimal cost, String barCode, String description,
			BigDecimal markup, int stock, Category category) {
		super();
		this.name = name;
		this.trademark = trademark;
		this.cost = cost;
		this.barCode = barCode;
		this.description = description;
		this.markup = markup;
		this.stock = stock;
		this.category = category;
	}

	@PrePersist
	public void prePersist() {
		this.active =true;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTrademark() {
		return trademark;
	}

	public void setTrademark(String trademark) {
		this.trademark = trademark;
	}

	public BigDecimal getCost() {
		return cost;
	}

	public void setCost(BigDecimal cost) {
		this.cost = cost;
	}

	public String getBarCode() {
		return barCode;
	}

	public void setBarCode(String barCode) {
		this.barCode = barCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public BigDecimal getMarkup() {
		return markup;
	}

	public void setMarkup(BigDecimal markup) {
		this.markup = markup;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	
	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public BigDecimal calculateProfit() {
		return this.markup == null || this.cost ==null ? new BigDecimal(0) 
				:(this.cost.multiply(this.markup)).divide(new BigDecimal(100));
	}

	public BigDecimal calculatePriceWithProfit() {
		BigDecimal profit = this.calculateProfit();
		return this.cost == null ? new BigDecimal(0) : this.cost.add(profit);
	}

	public BigDecimal calculateSalePrice() throws AppException {
		BigDecimal priceWithProfit = this.calculatePriceWithProfit();
		return priceWithProfit.equals(BigDecimal.ZERO) || priceWithProfit.compareTo(this.cost) == 0
				? new BigDecimal(0)
				:priceWithProfit.add(this.category.totalTaxesValue(priceWithProfit));// Preguntar si se calcula asi.
	}

	public void addStock(int quantity) {
		this.stock += quantity;
		
	}
	public void substractStock(int quantity) {
		this.stock -= quantity;
	}

	public void addProvider(Provider p) {
		if(!this.providers.contains(p)) {
			this.providers.add(p);
		}
	}
}
