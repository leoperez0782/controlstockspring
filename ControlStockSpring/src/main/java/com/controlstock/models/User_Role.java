package com.controlstock.models;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
@Entity
@Table(name = "user_role", uniqueConstraints= {@UniqueConstraint(columnNames= {"user_id", "role_id"})})
public class User_Role implements Serializable{

	private static final long serialVersionUID = 1L;
	@Id
	@ManyToOne
	private AppUser user;
	@Id
	@ManyToOne
	private Role role;
	public AppUser getUser() {
		return user;
	}
	public void setUser(AppUser user) {
		this.user = user;
	}
	public Role getRole() {
		return role;
	}
	public void setRole(Role role) {
		this.role = role;
	}
	
	

}
