package com.controlstock.models;

import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

/**
 * 
 * @author Leonardo Pérez
 * 8 mar. 2020
 */
@Entity
@Table(name="invoices")
@PrimaryKeyJoinColumn(name = "abstract_invoice_id")
public class Invoice extends AbstractInvoice{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@OneToOne(fetch=FetchType.LAZY, cascade= CascadeType.ALL)
	@JoinColumn(name="provider_id")
	private Provider provider;

	public Invoice() {
		super();
		
	}

	public Invoice(String series, Long number, LocalDate date, Provider provider) {
		super(series, number, date);
		this.provider = provider;
	}

	public Invoice(Provider provider) {
		super();
		this.provider = provider;
	}

	public Provider getProvider() {
		return provider;
	}

	public void setProvider(Provider provider) {
		this.provider = provider;
	}
	
	
}
