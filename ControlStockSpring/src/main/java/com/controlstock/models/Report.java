package com.controlstock.models;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
/**
 * 
 * @author Leonardo Pérez
 * 28 ago. 2020
 */
@Entity
@Table(name = "reports")
public class Report implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@OneToOne
	private CashRegister register;
	
	private BigDecimal total;
	private BigDecimal rechargesTotal;
	private BigDecimal lotteryTotal;
	private BigDecimal commonSalesTotal;
	
	
	public Report() {
		super();
		
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public CashRegister getRegister() {
		return register;
	}
	public void setRegister(CashRegister register) {
		this.register = register;
	}
	public BigDecimal getTotal() {
		return total;
	}
	public void setTotal(BigDecimal total) {
		this.total = total;
	}
	public BigDecimal getRechargesTotal() {
		return rechargesTotal;
	}
	public void setRechargesTotal(BigDecimal rechargesTotal) {
		this.rechargesTotal = rechargesTotal;
	}
	public BigDecimal getLotteryTotal() {
		return lotteryTotal;
	}
	public void setLotteryTotal(BigDecimal lotteryTotal) {
		this.lotteryTotal = lotteryTotal;
	}
	public BigDecimal getCommonSalesTotal() {
		return commonSalesTotal;
	}
	public void setCommonSalesTotal(BigDecimal commonSalesTotal) {
		this.commonSalesTotal = commonSalesTotal;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
}
