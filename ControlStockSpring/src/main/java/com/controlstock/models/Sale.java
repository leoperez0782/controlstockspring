package com.controlstock.models;

import java.time.LocalDate;
import java.time.LocalTime;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

/**
 * 
 * @author Leonardo Pérez
 * 8 mar. 2020
 */
@Entity
@Table(name="sales")
@PrimaryKeyJoinColumn(name = "abstract_invoice_id")
public class Sale extends AbstractInvoice{

	public enum PaymentMethod {EFECTIVO, DEBITO, CREDITO};
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private LocalTime saleTime;
	@Enumerated(EnumType.STRING)
	private PaymentMethod paymentMethod;
	
	public Sale() {
		super();
		this.saleTime = LocalTime.now();
		this.setDate(LocalDate.now());
		Long randomLong = 1L + (long) (Math.random() * (10L - 1L));//temporal hack
		this.setNumber(randomLong);
		this.setSeries("A");
		
	}

	public Sale(String series, Long number, LocalDate date) {
		super(series, number, date);
		
	}

	public LocalTime getSaleTime() {
		return saleTime;
	}

	public void setSaleTime(LocalTime saleTime) {
		this.saleTime = saleTime;
	}
//	@PrePersist
//	public void prePersist() {
//		this.saleTime = LocalTime.now();
//		this.setDate(LocalDate.now());
//		Long randomLong = 1L + (long) (Math.random() * (10L - 1L));//temporal hack
//		this.setNumber(randomLong);
//		this.setSeries("A");
//	}

	public PaymentMethod getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(PaymentMethod paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	
}
