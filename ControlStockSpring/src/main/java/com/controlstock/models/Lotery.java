package com.controlstock.models;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import com.controlstock.exceptions.AppException;

/**
 * 
 * @author Leonardo Pérez
 * 5 jun. 2020
 */
@Entity
@Table(name="lotery")
@PrimaryKeyJoinColumn(name = "product_id")
public class Lotery extends Product{

	private static final long serialVersionUID = 1L;
	private static final BigDecimal FIXED_VALUE = new BigDecimal(1);
	@Override
	public BigDecimal calculateProfit() {
		
		return FIXED_VALUE;
	}

	@Override
	public BigDecimal calculatePriceWithProfit() {
		
		return FIXED_VALUE;
	}

	@Override
	public BigDecimal calculateSalePrice() throws AppException {
		
		return FIXED_VALUE;
	}

	
}
