package com.controlstock.presenters;

import java.util.List;

import com.controlstock.exceptions.AppException;
import com.controlstock.models.Product;

public interface SalePresenterFactory {

	SalePresenter createFromProduct(Product p) throws AppException;
	List<SalePresenter> createFromList(List<Product> list);
}
