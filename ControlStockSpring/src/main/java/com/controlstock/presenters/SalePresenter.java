package com.controlstock.presenters;

import java.math.BigDecimal;

import com.controlstock.models.Sale.PaymentMethod;




public class SalePresenter {

	
	private Long id;
	
	private String name;
	private String barCode;
	private String trademark;
	
	private BigDecimal salePrice;
	private BigDecimal taxes;
	private BigDecimal priceWithOutTaxes;
	private int quantity;
	private PaymentMethod paymentMethod;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getBarCode() {
		return barCode;
	}
	public void setBarCode(String barCode) {
		this.barCode = barCode;
	}
	public String getTrademark() {
		return trademark;
	}
	public void setTrademark(String trademark) {
		this.trademark = trademark;
	}
	public BigDecimal getSalePrice() {
		return salePrice;
	}
	public void setSalePrice(BigDecimal salePrice) {
		this.salePrice = salePrice;
	}
	public BigDecimal getTaxes() {
		return taxes;
	}
	public void setTaxes(BigDecimal taxes) {
		this.taxes = taxes;
	}
	public BigDecimal getPriceWithOutTaxes() {
		return priceWithOutTaxes;
	}
	public void setPriceWithOutTaxes(BigDecimal priceWithOutTaxes) {
		this.priceWithOutTaxes = priceWithOutTaxes;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public PaymentMethod getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(PaymentMethod paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	
	
}
