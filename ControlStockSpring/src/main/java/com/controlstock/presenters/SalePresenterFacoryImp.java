package com.controlstock.presenters;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.controlstock.exceptions.AppException;
import com.controlstock.models.Product;

@Component
public class SalePresenterFacoryImp implements SalePresenterFactory{

	@Override
	public SalePresenter createFromProduct(Product p) throws AppException {
		SalePresenter presenter = new SalePresenter();
		presenter.setId(p.getId());
		presenter.setBarCode(p.getBarCode());
		presenter.setName(p.getName());
		presenter.setPriceWithOutTaxes(p.calculatePriceWithProfit());
		presenter.setQuantity(1);
		presenter.setSalePrice(p.calculateSalePrice());
		presenter.setTaxes(p.getCategory().totalTaxesValue(p.calculatePriceWithProfit()));
		presenter.setTrademark(p.getTrademark());
		return presenter;
	}

	@Override
	public List<SalePresenter> createFromList(List<Product> list) {
		List<SalePresenter> returnList = new ArrayList<>();
		System.out.println("Largo lista en presenter " + list.size());
		list.forEach(item -> {
			try {
				SalePresenter pres = createFromProduct(item);
				returnList.add(pres);
			} catch (AppException e) {
			
				e.printStackTrace();
				
			}
		});
		return returnList;
	}

}
