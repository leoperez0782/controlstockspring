package com.controlstock.presenters;

import java.util.ArrayList;
import java.util.List;

import com.controlstock.models.AppUser;
import com.controlstock.models.Role;
import com.controlstock.models.User_Role;

public class UserPresenter {

	private String username;
	private String password;
	private List<Role> roles;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<Role> getRoles() {
		return roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	public UserPresenter() {
		
	}
	public UserPresenter(AppUser user) {
		this.username = user.getUsername();
		this.roles = loadRoles(user);
	}
	public AppUser createUser() {
		AppUser user = new AppUser();
		user.setEnabled(true);
		user.setPassword(password);
		user.setUsername(username);
		for (Role r : roles) {
			user.addRole(r);

		}
		System.out.println("Roles :" + user.getRoles().size());
		return user;

	}

	

	private List<Role> loadRoles(AppUser user) {
		List<Role> userRoles = new ArrayList<>();
		for(User_Role r : user.getRoles()) {
			userRoles.add(r.getRole());
		}
		return userRoles;
	}
	
	public static List<UserPresenter> loadAppUsers(List<AppUser> usersList){
		List<UserPresenter> returnList = new ArrayList<>();
		for(AppUser user : usersList) {
			UserPresenter temp = new UserPresenter(user);
			returnList.add(temp);
		}
		return returnList;
	}
}
