package com.controlstock.presenters;

import java.math.BigDecimal;
import java.time.LocalDate;

public class CashRegisterPresenter {
	
	private BigDecimal commonSales;
	private BigDecimal loterySales;
	private BigDecimal rechargeSales;
	private BigDecimal total;
	private LocalDate  date;
	private String userName;
	
	/**
	 * @return the commonSales
	 */
	public BigDecimal getCommonSales() {
		return commonSales;
	}
	/**
	 * @param commonSales the commonSales to set
	 */
	public void setCommonSales(BigDecimal commonSales) {
		this.commonSales = commonSales.setScale(2);
	}
	/**
	 * @return the loterySales
	 */
	public BigDecimal getLoterySales() {
		return loterySales;
	}
	/**
	 * @param loterySales the loterySales to set
	 */
	public void setLoterySales(BigDecimal loterySales) {
		this.loterySales = loterySales;
	}
	
	
	/**
	 * @return the total
	 */
	public BigDecimal getTotal() {
		return total;
	}
	/**
	 * @param total the total to set
	 */
	public void setTotal(BigDecimal total) {
		this.total = total;
	}
	public BigDecimal totalSales() {
		this.total = commonSales.add(loterySales).add(rechargeSales);
		return this.total;
	}
	/**
	 * @return the rechargeSales
	 */
	public BigDecimal getRechargeSales() {
		return rechargeSales;
	}
	/**
	 * @param rechargeSales the rechargeSales to set
	 */
	public void setRechargeSales(BigDecimal rechargeSales) {
		this.rechargeSales = rechargeSales.setScale(2);
	}
	public LocalDate getDate() {
		return date;
	}
	public void setDate(LocalDate date) {
		this.date = date;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
}
