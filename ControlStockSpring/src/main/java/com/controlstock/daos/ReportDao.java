package com.controlstock.daos;

import org.springframework.data.repository.CrudRepository;

import com.controlstock.models.Report;

public interface ReportDao extends CrudRepository<Report, Long>{

}
