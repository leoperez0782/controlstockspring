package com.controlstock.daos;

import java.time.LocalDate;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.controlstock.models.CashRegister;

public interface CashRegisterDao extends CrudRepository<CashRegister, Long>{

	@Query("SELECT c FROM CashRegister c WHERE c.user.username = username AND c.date = date AND c.close = NULL")
	Optional<CashRegister> findByUserAndDate(String username, LocalDate date);
	
	//@Query("SELECT c FROM CashRegister c WHERE c.user.username = username AND c.date = date AND c.close IS NOT NULL")
	@Query(value =" SELECT * FROM cash_register c, users u  WHERE c.user_id = u.id AND c.close IS NOT NULL AND u.username = ?1 AND c.date = ?2",
			nativeQuery = true)
	Optional<CashRegister> findByUserAndDateAndClosed(String username, LocalDate date);
}
