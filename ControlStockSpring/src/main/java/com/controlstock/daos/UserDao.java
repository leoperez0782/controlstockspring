package com.controlstock.daos;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.controlstock.models.AppUser;

public interface UserDao extends CrudRepository<AppUser, Long>{
	Optional<AppUser> findByUsername(String username);
	List<AppUser> findAll();
}
