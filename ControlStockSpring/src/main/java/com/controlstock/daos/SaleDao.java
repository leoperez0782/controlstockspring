package com.controlstock.daos;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.controlstock.models.Sale;

public interface SaleDao extends CrudRepository<Sale, Long> {

	@Query("SELECT s FROM Sale s where s.saleTime BETWEEN ?1 AND ?2 AND s.date = ?3")
	List<Sale> findAllInTimeLapsus(LocalTime openTime, LocalTime closeTime, LocalDate date);
}
