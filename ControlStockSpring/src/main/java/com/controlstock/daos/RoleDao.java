package com.controlstock.daos;

import org.springframework.data.repository.CrudRepository;

import com.controlstock.models.Role;

public interface RoleDao extends CrudRepository<Role, Long>{

}
