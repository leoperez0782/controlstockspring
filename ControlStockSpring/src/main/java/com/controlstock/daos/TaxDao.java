package com.controlstock.daos;

import org.springframework.data.repository.CrudRepository;

import com.controlstock.models.Tax;

public interface TaxDao extends CrudRepository<Tax, Long>{

}
