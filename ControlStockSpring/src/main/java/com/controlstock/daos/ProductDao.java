package com.controlstock.daos;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.controlstock.models.Product;
/**
 * 
 * @author Leonardo Pérez
 * 4 abr. 2020
 */
public interface ProductDao extends CrudRepository<Product, Long>{

//	@Query("select p from Product p, Provider pro where pro.provider = ?1 and pro.products.contains(p)")
//	Iterable<Product> findAllByProvider(Provider prov);
	
	@Query("select p from Product p where p.name like %?1%")
	List<Product> findByNameLike(String term);
	@Query("select p from Product p where p.active = true")
	List<Product> findAllActives();
	@Query("select p from Product p where p.barCode = ?1")
	Optional<Product> findByBarCode(String barCode);
	
	@Query("select p from Product p where p.trademark = ?1")
	List<Product> findByTradeMark(String tradeMark);
}
