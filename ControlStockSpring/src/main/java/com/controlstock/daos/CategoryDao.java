package com.controlstock.daos;

import org.springframework.data.repository.CrudRepository;

import com.controlstock.models.Category;

public interface CategoryDao extends CrudRepository<Category, Long>{

}
