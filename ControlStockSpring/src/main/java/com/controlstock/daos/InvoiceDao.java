package com.controlstock.daos;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.controlstock.models.AbstractInvoice;


public interface InvoiceDao extends PagingAndSortingRepository<AbstractInvoice, Long>{

}
