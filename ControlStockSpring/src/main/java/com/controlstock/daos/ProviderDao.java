package com.controlstock.daos;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.controlstock.models.Provider;

public interface ProviderDao extends PagingAndSortingRepository<Provider, Long>{

}
