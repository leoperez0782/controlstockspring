package com.controlstock.services;

import java.util.List;
import java.util.Optional;

import com.controlstock.models.Product;
import com.controlstock.models.Provider;
/**
 * 
 * @author Leonardo Pérez
 * 4 abr. 2020
 */
public interface ProductService {
	Iterable<Product> findAllByProvider(Provider prov);
	void save(Product prod);
	List<Product> findByNameLikeIgnoreCase(String term);
	//List<Product> findByNameLikeIgnoreCaseActives(String term);
	Optional<Product> findById(Long id);
	List<Product> findAll();
	List<Product> findAllActives();
	Optional<Product> findByBarCode(String term);
	List<Product> findByTrademark(String term);
	
}
