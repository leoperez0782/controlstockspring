package com.controlstock.services;

import com.controlstock.models.AbstractInvoice;
/**
 * 
 * @author Leonardo Pérez
 * 4 abr. 2020
 */
public interface InvoiceService {
	void create(AbstractInvoice invoice);
}
