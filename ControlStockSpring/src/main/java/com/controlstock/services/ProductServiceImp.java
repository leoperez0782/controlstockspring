package com.controlstock.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.controlstock.daos.ProductDao;
import com.controlstock.models.Product;
import com.controlstock.models.Provider;
/**
 * 
 * @author Leonardo Pérez
 * 4 abr. 2020
 */
@Service
public class ProductServiceImp implements ProductService{

	@Autowired
	private ProductDao productDao;
	
	@Override
	@Transactional(readOnly = true)
	public Iterable<Product> findAllByProvider(Provider prov) {
		
		return null;
	}

	@Override
	@Transactional
	public void save(Product prod) {
		
		productDao.save(prod);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Product> findByNameLikeIgnoreCase(String term) {
		
		return productDao.findByNameLike(term);
	}

	@Override
	@Transactional(readOnly = true)
	public Optional<Product> findById(Long id) {
		
		return productDao.findById(id);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Product> findAll() {
		return (List<Product>) productDao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public List<Product> findAllActives() {
		return  productDao.findAllActives();
	}

	@Override
	@Transactional(readOnly = true)
	public Optional<Product> findByBarCode(String term) {
	
		return productDao.findByBarCode(term);
	}

	@Override
	public List<Product> findByTrademark(String term) {
		return productDao.findByTradeMark(term);
	}

	

}
