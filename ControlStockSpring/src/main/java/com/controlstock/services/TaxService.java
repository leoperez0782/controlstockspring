package com.controlstock.services;

import java.util.Optional;

import com.controlstock.models.Tax;


public interface TaxService {

	void save(Tax tax);
	Iterable<Tax> findAll();
	Optional<Tax> findById(Long id);
}
