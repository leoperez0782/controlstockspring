package com.controlstock.services;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.controlstock.daos.SaleDao;
import com.controlstock.models.Sale;

@Service
public class SaleServiceImp implements SaleService{

	@Autowired
	private SaleDao saleDao;
	@Override
	public void save(Sale sale) {
		
		saleDao.save(sale);
	}
	@Override
	public List<Sale> findAllInTimeLapsus(LocalTime openTime, LocalTime closeTime, LocalDate date) {
		return saleDao.findAllInTimeLapsus(openTime, closeTime, date);
	}

}
