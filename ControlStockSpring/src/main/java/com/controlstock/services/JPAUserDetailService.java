package com.controlstock.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.controlstock.daos.RoleDao;
import com.controlstock.daos.UserDao;
import com.controlstock.models.AppUser;
import com.controlstock.models.Role;
import com.controlstock.models.User_Role;

@Service("jpaUserDetailsService")
public class JPAUserDetailService implements UserDetailsService{

	private Logger logger = LoggerFactory.getLogger(JPAUserDetailService.class); 
	@Autowired
	private UserDao appUserDao;
	@Autowired
	private RoleDao roleDao;
	@Override
	@Transactional(readOnly = true)
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Optional<AppUser> userOp = appUserDao.findByUsername(username);
		if(userOp.isPresent()) {
			List<GrantedAuthority> authorities = new ArrayList<>();
			
			for(User_Role role : userOp.get().getRoles()) {
				authorities.add(new SimpleGrantedAuthority(role.getRole().getAuthority()));
			}
			AppUser appUser = userOp.get();
			return new User(appUser.getUsername(),appUser.getPassword(), appUser.isEnabled(), true, true, true,authorities);
		}else {
			logger.info("Error el usuario %s no existe", username);
			throw new UsernameNotFoundException(username);
		}
		
	}
	
	@Transactional(readOnly = true)
	public List<Role> findAll() {
		return (List<Role>) roleDao.findAll();
	}

	public void save(AppUser createUser) {
		
		appUserDao.save(createUser);
	}
	
	@Transactional(readOnly = true)
	public Optional<AppUser> findByUsername(String username){
		return appUserDao.findByUsername(username);
	}
	
	@Transactional(readOnly = true)
	public List<AppUser> findAllUsers(){
		return appUserDao.findAll();
	}

}
