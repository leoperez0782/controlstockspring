package com.controlstock.services;

import java.util.Optional;

import com.controlstock.models.Category;

public interface CategoryService {

	void create(Category category);

	Iterable<Category> findAll();
	Optional<Category> findById(Long id);
}
