package com.controlstock.services;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.controlstock.models.Provider;

public interface ProviderService {

	void create(Provider provider);
	Page<Provider> findAll(Pageable pageable);
	Optional<Provider> findById(Long id);
}
