package com.controlstock.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.controlstock.daos.CategoryDao;
import com.controlstock.models.Category;

@Service
public class CategoryServiceImp implements CategoryService{
	@Autowired
	private CategoryDao categoryDao;

	@Override
	public void create(Category category) {
		categoryDao.save(category);
	}

	@Override
	public Iterable<Category> findAll() {
		return categoryDao.findAll();
	}

	@Override
	public Optional<Category> findById(Long id) {
		return categoryDao.findById(id);
	}

}
