package com.controlstock.services;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

import com.controlstock.models.Sale;

public interface SaleService {

	void save(Sale sale);
	List<Sale> findAllInTimeLapsus(LocalTime openTime, LocalTime closeTime, LocalDate date);
}
