package com.controlstock.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.controlstock.daos.ProviderDao;
import com.controlstock.models.Provider;
@Service
public class ProviderServiceImp implements ProviderService{

	@Autowired
	private ProviderDao providerDao;
	
	@Transactional
	@Override
	public void create(Provider provider)   {
		
			providerDao.save(provider);
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Provider> findAll(Pageable pageable) {
		
		return providerDao.findAll(pageable);
	}

	@Override
	public Optional<Provider> findById(Long id) {
		return providerDao.findById(id);
	}

	
}
