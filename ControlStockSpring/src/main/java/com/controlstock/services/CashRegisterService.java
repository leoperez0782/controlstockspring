package com.controlstock.services;

import java.time.LocalDate;
import java.util.Optional;

import com.controlstock.models.CashRegister;

public interface CashRegisterService {
	
	CashRegister save(CashRegister entity);
	Optional<CashRegister> findByUserAndDate(String username, LocalDate date);
	Optional<CashRegister> findByUserAndDateAndClosed(String username, LocalDate date);
	
	
}
