package com.controlstock.services;

import java.time.LocalDate;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.controlstock.daos.CashRegisterDao;
import com.controlstock.models.CashRegister;


@Service
public class CashRegisterImp implements CashRegisterService{

	@Autowired
	private CashRegisterDao cashRegisterDao;
	
	@Override
	public CashRegister save(CashRegister entity) {
		return cashRegisterDao.save(entity);
	}

	@Override
	public Optional<CashRegister> findByUserAndDate(String username, LocalDate date) {
		
		return cashRegisterDao.findByUserAndDate(username, date);
	}

	@Override
	public Optional<CashRegister> findByUserAndDateAndClosed(String username, LocalDate date) {
		
		return cashRegisterDao.findByUserAndDateAndClosed(username, date);
	}

	
	
	

}
