package com.controlstock.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.controlstock.daos.ReportDao;
import com.controlstock.models.Report;

@Service
public class ReportServiceImp implements ReportService{

	@Autowired
	private ReportDao reportDao;
	
	@Override
	public void create(Report report) {
		reportDao.save(report);
	}

}
