package com.controlstock.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.controlstock.daos.TaxDao;
import com.controlstock.models.Tax;
@Service
public class TaxServiceImp implements TaxService{

	@Autowired
	private TaxDao taxDao;
	@Override
	public void save(Tax tax) {
		
		taxDao.save(tax);
		
	}

	@Override
	public Iterable<Tax> findAll() {
		return taxDao.findAll();
	}

	@Override
	public Optional<Tax> findById(Long id) {
		return taxDao.findById(id);
	}

}
