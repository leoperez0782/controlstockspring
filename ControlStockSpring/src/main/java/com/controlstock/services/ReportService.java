package com.controlstock.services;

import com.controlstock.models.Report;

public interface ReportService {

	void create(Report report);
}
