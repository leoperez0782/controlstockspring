package com.controlstock.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.controlstock.daos.InvoiceDao;
import com.controlstock.models.AbstractInvoice;
@Service
public class InvoiceServiceImp implements InvoiceService{

	@Autowired
	private InvoiceDao invoiceDao;
	@Override
	public void create(AbstractInvoice invoice) {

		invoiceDao.save(invoice);
		
	}

}
