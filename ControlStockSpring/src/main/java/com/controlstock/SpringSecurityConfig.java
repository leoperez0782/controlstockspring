package com.controlstock;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.controlstock.auth.handler.LoginSuccessHandler;
import com.controlstock.services.JPAUserDetailService;
@EnableGlobalMethodSecurity(securedEnabled = true)
@Configuration
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter{

	@Autowired
	JPAUserDetailService userDetailsService;
	@Autowired
	private LoginSuccessHandler successHandler;
	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
	@Autowired
	public void configurerGlobal(AuthenticationManagerBuilder builder) throws Exception {
//		PasswordEncoder encoder = passwordEncoder();
//		UserBuilder users = User.builder().passwordEncoder(encoder::encode);
//		builder.inMemoryAuthentication()
//		.withUser(users.username("admin").password("12345").roles("ADMIN","USER"))
//		.withUser(users.username("leo").password("12345").roles("USER"));
//		System.out.println(encoder.encode("12345"));
		builder.userDetailsService(userDetailsService)
		.passwordEncoder(passwordEncoder());
	}
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests().antMatchers( "/css/**", "/js/**").permitAll()
//		.antMatchers("/categorias/**").hasAnyRole("ADMIN")
//		//.antMatchers("/index.html").hasAnyRole("ADMIN", "USER")
//		.antMatchers("/proveedor/**", "/productos/**", "/facturas/**", "/impuestos/**").hasAnyRole("ADMIN")
//		.antMatchers("/ventas/**").hasAnyRole("ADMIN","USER")
		.anyRequest().authenticated()
		.and()
		.formLogin()
			.successHandler(successHandler)
			.loginPage("/login")
		
		.permitAll()
		.and()
		.logout().permitAll()
		.and()
		.exceptionHandling().accessDeniedPage("/error-403");
	}
	
	
}
