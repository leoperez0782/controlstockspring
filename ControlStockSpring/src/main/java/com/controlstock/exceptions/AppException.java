package com.controlstock.exceptions;

public class AppException extends Exception {

	public AppException(String mensaje) {
		super(mensaje);
	}
	
	public AppException() {
		super();
		
	}

	public AppException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		
	}

	public AppException(String message, Throwable cause) {
		super(message, cause);
		
	}

	public AppException(Throwable cause) {
		super(cause);
		
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
