package com.controlstock.interactors;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import org.springframework.stereotype.Component;

import com.controlstock.exceptions.AppException;
import com.controlstock.models.Sale;
@Component
public class PhoneRechargeSalesCalculator {

	BigDecimal total = new BigDecimal(0).setScale(2,RoundingMode.HALF_UP);
	public BigDecimal totalRechargeSales(List<Sale> sales) {
		
		sales.forEach(item -> 
		item.getLines().forEach(line -> {
			if(line.getProduct().getCategory().getCategoryName().equals("Recargas")) {
				try {
					total = total.add(line.calculateTotal());
				} catch (AppException e) {
					
					e.printStackTrace();
				}
			}
		})
	);
		return total;
	}
}
