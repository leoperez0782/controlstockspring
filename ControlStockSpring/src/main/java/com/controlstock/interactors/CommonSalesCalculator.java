package com.controlstock.interactors;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import com.pivovarit.function.ThrowingFunction;
import com.controlstock.exceptions.AppException;
import com.controlstock.models.Line;
import com.controlstock.models.Sale;

import org.springframework.stereotype.Component;
/**
 * Clase utilizada para calcular el total de ventas sin Loterias y quinielas.
 * @author Leonardo Pérez
 * 1 jun. 2021
 */
@Component
public class CommonSalesCalculator {

	BigDecimal total = new BigDecimal(0).setScale(2, RoundingMode.HALF_UP);

	public BigDecimal totalSalesWithOutLotery(List<Sale> sales) {

		sales.forEach(item -> item.getLines().forEach(line -> {
			if (!(line.getProduct().getCategory().getCategoryName().equals("Loterias y quinielas"))
					&& !(line.getProduct().getCategory().getCategoryName().equals("Recargas"))) {
				try {
					total = total.add(line.calculateTotal());
				} catch (AppException e) {

					System.out.println(e.getMessage());

				}
			}
		}));
//		Predicate<Line> loteriasYQuinielas = ln -> ln.getProduct().getCategory().getCategoryName()
//				.equals("Loterias y quinielas");
//		Predicate<Line> recargas = ln -> ln.getProduct().getCategory().getCategoryName().equals("Recargas");
//
//		sales.stream().forEach(sale -> 
//			sale.getLines().stream()
//			.filter((loteriasYQuinielas.negate().and(recargas).negate()))
//					.map(ThrowingFunction.unchecked(Line::calculateTotal))
//					.reduce(total, BigDecimal::add)
//		);

		return total;
	}
}
