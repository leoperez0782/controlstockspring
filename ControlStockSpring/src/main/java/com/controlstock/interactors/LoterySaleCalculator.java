package com.controlstock.interactors;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.List;

import org.springframework.stereotype.Component;

import com.controlstock.exceptions.AppException;

import com.controlstock.models.Sale;

@Component
public class LoterySaleCalculator {

	BigDecimal total = new BigDecimal(0, new MathContext(2, RoundingMode.HALF_UP));
	public BigDecimal calculateLoterySales(List<Sale> sales)  {
		
		sales.forEach(item -> 
			item.getLines().forEach(line -> {
				if(line.getProduct().getCategory().getCategoryName().equals("Loterias y quinielas")) {
					try {
						total = total.add(line.calculateTotal());
					} catch (AppException e) {
						
						e.printStackTrace();
					}
				}
			})
		);
		return total;
	}
}
