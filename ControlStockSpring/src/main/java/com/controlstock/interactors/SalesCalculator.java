package com.controlstock.interactors;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.controlstock.exceptions.AppException;
import com.controlstock.models.Line;
import com.controlstock.models.Sale;
import com.controlstock.presenters.CashRegisterPresenter;

@Component
public class SalesCalculator {

	

	// cambiar usando el contador pattern.
	public void calculateSales(CashRegisterPresenter presenter, List<Sale> sales) throws AppException {
		BigDecimal loterySales = new BigDecimal(0).setScale(2);
		BigDecimal commonSales = new BigDecimal(0).setScale(2);
		BigDecimal rechargeSales = new BigDecimal(0).setScale(2);
		BigDecimal totalSales;
		

		for (Sale sale : sales) {
			for (Line line : sale.getLines()) {
				String categoryName = line.getProduct().getCategory().getCategoryName().toLowerCase();
				switch (categoryName) {
				case "loterias y quinielas":
					loterySales = loterySales.add(line.calculateTotal());
					break;
				case "recargas":
					rechargeSales = rechargeSales.add(line.calculateTotal());
					break;
				default:
					commonSales = commonSales.add(line.calculateTotal());
					break;
				}
			}
		}
		
		totalSales = commonSales.add(rechargeSales).add(loterySales);
		//Add data to presenter
		presenter.setCommonSales(commonSales);
		presenter.setLoterySales(loterySales);
		presenter.setRechargeSales(rechargeSales);
		presenter.setTotal(totalSales);

	}

//	public void calculateSales(Report report, CashRegister register) throws AppException {
//		BigDecimal commonSales = commonSalesCalculator.totalSalesWithOutLotery(register.getSales());
//		BigDecimal lotterySales = loterySaleCalculator.calculateLoterySales(register.getSales());
//		BigDecimal rechargeSales = rechargeSalesCalculator.totalRechargeSales(register.getSales());
//		BigDecimal total = commonSales.add(lotterySales).add(rechargeSales);
//		report.setCommonSalesTotal(commonSales);
//		report.setLotteryTotal(lotterySales);
//		report.setRechargesTotal(rechargeSales);
//		report.setTotal(total);
//	}
}
